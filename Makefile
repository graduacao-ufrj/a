# Compiler
CXX := clang++

# Compiler flags
CXXFLAGS := -std=c++14 -Wall

EXECUTABLE = TicketOs.x

# Get source files from TicketOs folder and the main.cpp in the root folder
SOURCE_FILES = $(wildcard TicketOs/domain/*.cc) $(wildcard TicketOs/infrastructure/**/*.cc) $(wildcard TicketOs/utility/*.cc) main.cc

# Object files
OBJECT_FILES = $(SOURCE_FILES:.cc=.o)

# Objects directory
OBJECTS_DIR = objects

# Data directory
DATA_DIR = data

# Rule to compile the executable
$(EXECUTABLE): $(OBJECT_FILES)
	$(CXX) $(CXXFLAGS) $(OBJECTS_DIR)/*.o -o $@

# Rule to compile the object files
%.o: %.cc | $(OBJECTS_DIR) $(DATA_DIR)
	$(CXX) $(CXXFLAGS) $(INCLUDE_DIRS) -c $< -o $@
	mv $@ objects/

$(OBJECTS_DIR):
	mkdir -p $@

$(DATA_DIR):
	mkdir -p $@

# Rule to clean object files and executable
clean:
	rm -f $(OBJECTS_DIR)/*.o $(DATA_DIR)/* $(EXECUTABLE) queue.x barbeiro.x
	rmdir $(OBJECTS_DIR) $(DATA_DIR)

clean_data:
	rm -f $(DATA_DIR)/*
	rmdir $(DATA_DIR)

clean_objects:
	rm -f $(OBJECTS_DIR)/*
	rmdir $(OBJECTS_DIR)

# Rule to run unit tests
test:
	bazel test --test_output=all --cxxopt=-std=c++14 //:unit_tests

queue.x: queue.cc
	clang++ $(CXXFLAGS) $^ -o $@

queueRoundRobin.x: queueRoundRobin.cc
	clang++ $(CXXFLAGS) $^ -o $@

queueMLFQ.x: queueMLFQ.cc
	clang++ $(CXXFLAGS) $^ -o $@

queueSCF.x: queueSCF.cc
	clang++ $(CXXFLAGS) $^ -o $@

barbeiro.x: barbeiro.cc
	clang++ $(CXXFLAGS) $^ -o $@