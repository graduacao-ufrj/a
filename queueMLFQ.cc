#include <iostream>
#include <stdlib.h>
#include <chrono>
#include <thread>
#include <mutex>
#include <condition_variable>

std::mutex printLock;

std::mutex concurrentLock;
std::condition_variable_any concurrentCondition;

struct Client {
    int id = 0;
    int usageTime = 0;
    int remainingTime = 0; // Adiciona tempo restante para cada cliente
    int priority = 0; // Adiciona prioridade para cada cliente
    std::chrono::time_point<std::chrono::system_clock> *arrivalTime;
    std::chrono::time_point<std::chrono::system_clock> *responseTime;
    std::chrono::time_point<std::chrono::system_clock> *conclusionTime;
    struct Client *next = NULL;
};

struct Queue {
    struct Client *head = NULL;
    struct Client *tail = NULL;
    int size = 0;
};

Queue finishedClients = {NULL, NULL, 0};

std::ostream &printAlone(const char *message) {
    printLock.lock();
    std::cout << message;
    printLock.unlock();
    return std::cout;
}

void enqueue(struct Queue *queue, struct Client *client) {
    // Adiciona um cliente à fila
    if (queue->head == NULL) {
        queue->head = client;
        queue->tail = client;
        queue->size = 1;
    } else {
        queue->tail->next = client;
        queue->tail = client;
        queue->size++;
    }
}

struct Client *dequeue(struct Queue *queue) {
    // Remove e retorna o próximo cliente da fila
    struct Client *client = queue->head;
    if (client != NULL) {
        queue->head = client->next;
        if (queue->head == NULL) {
            queue->tail = NULL;
        }
        queue->size--;
    }
    return client;
}

void processClient(
    struct Client *client,
    int *numberOfConcurrentClients,
    int *numberOfFinishedClients,
    int maximumNumberOfConcurrentClients,
    int timeQuantum,
    struct Queue *queueA,
    struct Queue *queueB,
    struct Queue *queueC,
    int fairSlice
) {
    concurrentLock.lock();

    if (client->arrivalTime == NULL) {
        client->arrivalTime = new std::chrono::time_point<std::chrono::system_clock>(std::chrono::system_clock::now());
    }

    while (*numberOfConcurrentClients >= maximumNumberOfConcurrentClients) {
        printAlone("\tAguardando um cliente terminar\n");
        concurrentCondition.wait(concurrentLock);
    }

    if (client->responseTime == NULL) {
        client->responseTime = new std::chrono::time_point<std::chrono::system_clock>(std::chrono::system_clock::now());
    }

    *numberOfConcurrentClients += 1;
    printAlone("\tNúmero de clientes simultâneos: ") << *numberOfConcurrentClients << "\n";
    concurrentLock.unlock();

    printAlone("Cliente ") << client->id << " com tempo de uso " << client->usageTime << " iniciou\n";
    int executionTime = std::min(client->remainingTime, timeQuantum);
    std::this_thread::sleep_for(std::chrono::milliseconds(executionTime));
    client->remainingTime -= executionTime;

    if (client->remainingTime > 0) {
        printAlone("Cliente ") << client->id << " não terminou, tempo restante: " << client->remainingTime << "\n";
        // Cria um novo cliente com os mesmos dados para reenfileirar
        struct Client *newClient = (struct Client *) malloc(sizeof(struct Client));
        newClient->id = client->id;
        newClient->usageTime = client->usageTime;
        newClient->remainingTime = client->remainingTime; // Atualiza o remainingTime
        newClient->priority = client->priority; // Mantém a mesma prioridade
        newClient->next = NULL;
        newClient->arrivalTime = client->arrivalTime;
        newClient->responseTime = client->responseTime;
        newClient->conclusionTime = client->conclusionTime;

        // Move o cliente para a próxima fila de prioridade mais baixa
        if (client->priority == 1) {
            enqueue(queueB, newClient);
        } else if (client->priority == 2) {
            enqueue(queueC, newClient);
        } else {
            enqueue(queueC, newClient);
        }
    } else {
        printAlone("Cliente ") << client->id << " com tempo de uso " << client->usageTime << " terminou\n";
        client->conclusionTime = new std::chrono::time_point<std::chrono::system_clock>(std::chrono::system_clock::now());
        enqueue(&finishedClients, client);
        //delete client; // Deleta o cliente corretamente
        *numberOfFinishedClients += 1;
    }

    concurrentLock.lock();
    *numberOfConcurrentClients -= 1;
    concurrentCondition.notify_one();
    concurrentLock.unlock();
}

void roundRobin(struct Queue *queueA, struct Queue *queueB, struct Queue *queueC, int maximumNumberOfConcurrentClients, int timeQuantum, int fairSlice) {
    int numberOfConcurrentClients = 0;
    int numberOfFinishedClients = 0;
    const int totalNumberOfClients = queueA->size + queueB->size + queueC->size;
    std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();

    while (numberOfFinishedClients < totalNumberOfClients) {
        struct Client *client = NULL;

        // Select the highest priority queue with a client
        if (queueA->size > 0) {
            client = dequeue(queueA);
        } else if (queueB->size > 0) {
            client = dequeue(queueB);
        } else if (queueC->size > 0) {
            client = dequeue(queueC);
        }

        if (client != NULL) {
            std::thread t1(processClient, client, &numberOfConcurrentClients, &numberOfFinishedClients, maximumNumberOfConcurrentClients, timeQuantum, queueA, queueB, queueC, fairSlice);
            t1.detach();
        }

        // Move all tasks to the highest priority queue after a time S
        if (numberOfFinishedClients % fairSlice == 0 && numberOfFinishedClients > 0) {
            while (queueB->size > 0) {
                struct Client *clientB = dequeue(queueB);
                clientB->priority = 1;
                enqueue(queueA, clientB);
            }
            while (queueC->size > 0) {
                struct Client *clientC = dequeue(queueC);
                clientC->priority = 1;
                enqueue(queueA, clientC);
            }
        }

        // // Esperar um pouco antes de checar novamente para evitar loop muito rápido
        // std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    while (numberOfConcurrentClients > 0) {
        printAlone("\tAguardando todos os clientes terminarem\n");
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

    std::chrono::time_point<std::chrono::system_clock> end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsedSeconds = end - start;
    printAlone("Tempo decorrido: ") << elapsedSeconds.count() << "s\n";

    int meanResponseTime = 0;
    int meanConclusionTime = 0;
    int starvedClients = 0;
    for (struct Client *client = finishedClients.head; client != NULL; client = client->next) {
        int responseTime = std::chrono::duration_cast<std::chrono::milliseconds>(*(client->responseTime) - *(client->arrivalTime)).count();
        meanResponseTime += responseTime;
        int conclusionTime = std::chrono::duration_cast<std::chrono::milliseconds>(*(client->conclusionTime) - *(client->arrivalTime)).count();
        meanConclusionTime += conclusionTime;

        if (responseTime > client->usageTime * 5) {
            starvedClients++;
        }
    }
    meanResponseTime /= totalNumberOfClients;
    meanConclusionTime /= totalNumberOfClients;

    printAlone("Tempo médio de resposta: ") << meanResponseTime << "ms\n";
    printAlone("Tempo médio de conclusão: ") << meanConclusionTime << "ms\n";
    printAlone("Clientes em starvation: ") << starvedClients << "\n";
}

int main(int argc, char **argv) {
    if (argc != 5) {
        printAlone("Uso: ") << argv[0] << " <numero_de_clientes> <numero_maximo_de_clientes_simultaneos> <quantum_de_tempo> <fair_slice>\n";
        return 1;
    }

    char *end;
    int numberOfClients = (int) strtol(argv[1], &end, 10);
    if (*end != '\0') {
        printAlone("O número de clientes deve ser numérico\n");
        return 1;
    }

    int maximumNumberOfConcurrentClients = (int) strtol(argv[2], &end, 10);
    if (*end != '\0') {
        printAlone("O número máximo de clientes simultâneos deve ser numérico\n");
        return 1;
    }

    int timeQuantum = (int) strtol(argv[3], &end, 10);
    if (*end != '\0') {
        printAlone("O quantum de tempo deve ser numérico\n");
        return 1;
    }

    int fairSlice = (int) strtol(argv[4], &end, 10);
    if (*end != '\0') {
        printAlone("O fair slice deve ser numérico\n");
        return 1;
    }

    printAlone("Número disponível de threads: ") << std::thread::hardware_concurrency() << "\n";
    if (maximumNumberOfConcurrentClients > std::thread::hardware_concurrency()) {
        maximumNumberOfConcurrentClients = std::thread::hardware_concurrency();
        printAlone("Número máximo de clientes simultâneos ajustado para ") << maximumNumberOfConcurrentClients << "\n";
    }

    struct Queue queueA;
    struct Queue queueB;
    struct Queue queueC;

    srand(time(NULL));
    for (int i = 0; i < numberOfClients; i++) {
        struct Client *client = (struct Client *) malloc(sizeof(struct Client));
        client->id = i + 1;
        client->usageTime = rand() % 100 + 1;
        client->remainingTime = client->usageTime; // Inicializa o tempo restante
        client->priority = 0; // Inicializa a prioridade como a mais baixa
        client->next = NULL;
        client->arrivalTime = NULL;
        client->responseTime = NULL;
        client->conclusionTime = NULL;
        enqueue(&queueA, client);
    }

    std::thread t1(roundRobin, &queueA, &queueB, &queueC, maximumNumberOfConcurrentClients, timeQuantum, fairSlice);
    t1.join();

    for (struct Client *client = queueA.head; client != NULL; client = client->next) {
        delete client->arrivalTime;
        if (client->responseTime != NULL) {
            delete client->responseTime;
        }
        if (client->conclusionTime != NULL) {
            delete client->conclusionTime;
        }
        free(client);
    }
}
