#include <iostream>
#include <stdlib.h>
#include <chrono>
#include <thread>
#include <mutex>
#include <condition_variable>

std::mutex printLock;

std::mutex concurrentLock;
std::condition_variable_any concurrentCondition;

struct Client {
    int id = 0;
    int usageTime = 0;
    struct Client *next = NULL;
};

struct Queue {
    struct Client *head = NULL;
    struct Client *tail = NULL;
    int size = 0;
};

std::ostream &printAlone(const char *message) {
    printLock.lock();
    std::cout << message;
    printLock.unlock();
    return std::cout;
}

void enqueue(struct Queue *queue, struct Client *client) {
    if (queue->head == NULL) {
        queue->head = client;
        queue->tail = client;
        queue->size = 1;
    } else {
        queue->tail->next = client;
        queue->tail = client;
        queue->size++;
    }
}

struct Client *dequeue(struct Queue *queue) {
    struct Client *client = queue->head;
    if (client != NULL) {
        queue->head = client->next;
        queue->size--;
    }
    return client;
}

void processClient(
    struct Client *client,
    int *numberOfConcurrentClients,
    int *numberOfFinishedClients,
    int maximumNumberOfConcurrentClients
) {
    concurrentLock.lock();
    while (*numberOfConcurrentClients >= maximumNumberOfConcurrentClients) {
        printAlone("\tWaiting for a client to finish\n");
        concurrentCondition.wait(concurrentLock);
    }

    *numberOfConcurrentClients += 1;
    printAlone("\tNumber of concurrent clients: ") << *numberOfConcurrentClients << "\n";
    concurrentLock.unlock();

    printAlone("Client ") << client->id << " with usage time " << client->usageTime << " started\n";
    std::this_thread::sleep_for(std::chrono::milliseconds(client->usageTime));
    printAlone("Client ") << client->id << " with usage time " << client->usageTime << " finished\n";

    concurrentLock.lock();
    *numberOfConcurrentClients -= 1;
    *numberOfFinishedClients += 1;
    concurrentCondition.notify_one();
    concurrentLock.unlock();
}

void firstComeFirstServed(struct Queue *queue, int maximumNumberOfConcurrentClients) {
    int numberOfConcurrentClients = 0;
    int numberOfFinishedClients = 0;
    const int totalNumberOfClients = queue->size;
    std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();

    while (queue->size > 0) {
        struct Client *client = dequeue(queue);

        if (client != NULL) {
            std::thread t1(processClient, client, &numberOfConcurrentClients, &numberOfFinishedClients, maximumNumberOfConcurrentClients);
            t1.detach();
        }
    }

    while (numberOfFinishedClients < totalNumberOfClients) {
        printAlone("\tWaiting for all clients to finish\n");
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

    std::chrono::time_point<std::chrono::system_clock> end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsedSeconds = end - start;
    printAlone("Elapsed time: ") << elapsedSeconds.count() << "s\n";
}



int main(int argc, char **argv) {
    if (argc != 3) {
        printAlone("Usage: ") << argv[0] << " <number_of_clients> <maximum_number_of_concurrent_clients>\n";
        return 1;
    }

    char *end;
    int numberOfClients = (int) strtol(argv[1], &end, 10);
    if (*end != '\0') {
        printAlone("Number of clients must be numeric\n");
        return 1;
    }

    int maximumNumberOfConcurrentClients = (int) strtol(argv[2], &end, 10);
    if (*end != '\0') {
        printAlone("Maximum number of concurrent clients must be numeric\n");
        return 1;
    }

    printAlone("Available number of threads: ") << std::thread::hardware_concurrency() << "\n";
    if (maximumNumberOfConcurrentClients > std::thread::hardware_concurrency()) {
        maximumNumberOfConcurrentClients = std::thread::hardware_concurrency();
        printAlone("Maximum number of concurrent clients adjusted to ") << maximumNumberOfConcurrentClients << "\n";
    }

    struct Queue queue;

    srand(time(NULL));
    for (int i = 0; i < numberOfClients; i++) {
        struct Client *client = (struct Client *) malloc(sizeof(struct Client));
        client->id = i + 1;
        client->usageTime = rand() % 100 + 1;
        client->next = NULL;
        enqueue(&queue, client);
    }

    std::thread t1(firstComeFirstServed, &queue, maximumNumberOfConcurrentClients);

    t1.join();
}