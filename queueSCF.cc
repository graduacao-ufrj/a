#include <thread>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <cstdint>
#include <random>
#include <list>
#include <iostream>

// --------- Constants ---------
// === Error Codes ===
#define OK 0
#define WRONG_ARGUMENTS 1
#define INVALID_NUMBER_OF_CLIENTS 2
#define INVALID_NUMBER_OF_CONCURRENT_SPOTS 3
#define HARDWARE_CONCURRENCY_EXCEEDED 4
#define SCHEDULER_FAILED 5

// --------- Data structures ---------
struct Client {
    int id;
    std::chrono::milliseconds duration;
    std::chrono::milliseconds originalDuration;
    std::chrono::time_point<std::chrono::system_clock> arrivalTime;
    std::chrono::time_point<std::chrono::system_clock> responseTime;
    std::chrono::time_point<std::chrono::system_clock> conclusionTime;
    struct Client *next;
    struct Client *prev;
};

struct Clientele {
    int size;
    struct Client *head;
    struct Client *tail;
};

struct Thread {
    std::thread thread;
    struct Thread *next;
    struct Thread *prev;
};

struct ThreadPool {
    int size;
    struct Thread *head;
    struct Thread *tail;
};

// --------- Functions headers ---------
int shortestCompletionFirst(Clientele *queue, int numberOfConcurrentSpots, std::mutex &queueLock, std::condition_variable_any &queueChangeCondition);
void clientDispatcher(int id, Clientele *queue, std::mutex &queueLock, std::condition_variable_any &queueChangeCondition);
void clientWorker(int id, Client *client, std::mutex &queueLock, std::condition_variable_any &queueChangeCondition);

template <typename listType, typename itemType>
listType *buildEmptyList();
template <typename listType, typename itemType>
int enqueue(listType *list, itemType *item);
template <typename listType, typename itemType>
int dequeue(listType *list, itemType *item);

// Global variables
std::mutex printLock;
std::mutex cleanupLock;
std::list<Client *> finishedClients;

// --------- Main ---------
int main (int argc, char **argv) {
    // --------- Argument parsing ---------
    if (argc < 3) {
        printLock.lock();
        std::cout << "Usage: " << argv[0] << " <number of clients> <number of concurrent spots>" << std::endl;
        printLock.unlock();
        exit(WRONG_ARGUMENTS);
    }

    std::random_device randomDevice;
    std::mt19937 randomEngine(randomDevice());
    std::uniform_int_distribution<int> randomDistribution(1, 100);

    char *endptr = new char;

    int numberOfClients = std::strtoul(argv[1], &endptr, 10);
    if (*endptr != '\0') {
        printLock.lock();
        std::cerr << "Invalid number of clients" << std::endl;
        printLock.unlock();

        delete endptr;
        exit(INVALID_NUMBER_OF_CLIENTS);
    }

    int numberOfConcurrentSpots = std::strtoul(argv[2], &endptr, 10);
    if (*endptr != '\0') {
        printLock.lock();
        std::cerr << "Invalid number of concurrent spots" << std::endl;
        printLock.unlock();

        delete endptr;
        exit(INVALID_NUMBER_OF_CONCURRENT_SPOTS);
    }

    if (numberOfConcurrentSpots > std::thread::hardware_concurrency()) {
        printLock.lock();
        std::cerr << "Number of concurrent spots exceeds hardware concurrency" << std::endl;
        std::cerr << "Hardware concurrency: " << std::thread::hardware_concurrency() << std::endl;
        printLock.unlock();

        delete endptr;
        exit(HARDWARE_CONCURRENCY_EXCEEDED);
    }

    // --------- Initialization ---------
    struct Clientele *clientele = buildEmptyList<Clientele, Client>();

    for (int i = 0; i < numberOfClients; i++) {
        struct Client *client = new struct Client;
        client->id = i;
        client->duration = std::chrono::milliseconds(randomDistribution(randomEngine));
        client->originalDuration = client->duration;
        client->next = nullptr;
        client->prev = nullptr;

        enqueue<Clientele, Client>(clientele, client);
    }

    // --------- Mutex and condition variable ---------
    std::mutex queueLock;
    std::condition_variable_any queueChangeCondition;

    // --------- Scheduler ---------
    printLock.lock();
    std::cout << "Starting shortest completion first scheduler" << std::endl;
    printLock.unlock();
    std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();
    int status = shortestCompletionFirst(
        clientele,
        numberOfConcurrentSpots,
        std::ref(queueLock),
        std::ref(queueChangeCondition)
    );

    if (status != 0) {
        printLock.lock();
        std::cerr << "Scheduler failed" << std::endl;
        std::cerr << "Status: " << status << std::endl;
        printLock.unlock();
        exit(SCHEDULER_FAILED);
    }

    std::chrono::time_point<std::chrono::system_clock> end = std::chrono::system_clock::now();

    printLock.lock();
    std::cout << "Scheduler finished" << std::endl;
    std::cout << "Duration: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << "ms" << std::endl;

    int meanResponseTime = 0;
    int meanConclusionTime = 0;
    int starvedClients = 0;
    for (Client *client : finishedClients) {
        int responseTime = std::chrono::duration_cast<std::chrono::milliseconds>(client->responseTime - client->arrivalTime).count();
        int conclusionTime = std::chrono::duration_cast<std::chrono::milliseconds>(client->conclusionTime - client->arrivalTime).count();
        std::cout << "\tClient " << (int) client->id << " arrived at " << std::chrono::duration_cast<std::chrono::milliseconds>(client->arrivalTime - start).count() << "ms" << std::endl;
        std::cout << "\tClient " << (int) client->id << " responded at " << std::chrono::duration_cast<std::chrono::milliseconds>(client->responseTime - start).count() << "ms" << std::endl;
        std::cout << "\tClient " << (int) client->id << " concluded at " << std::chrono::duration_cast<std::chrono::milliseconds>(client->conclusionTime - start).count() << "ms" << std::endl;
        std::cout << "\tClient " << (int) client->id << " response time: " << responseTime << "ms" << std::endl;
        std::cout << "\tClient " << (int) client->id << " conclusion time: " << conclusionTime << "ms" << std::endl;

        if (responseTime > 5 * client->originalDuration.count()) {
            std::cout << "\tClient duration: " << client->originalDuration.count() << "ms" << std::endl; 
            std::cout << "\tClient " << (int) client->id << " starved" << std::endl;
            starvedClients++;
        }

        meanResponseTime += responseTime;
        meanConclusionTime += conclusionTime;
    }

    meanResponseTime /= finishedClients.size();
    meanConclusionTime /= finishedClients.size();
    std::cout << "Mean response time: " << meanResponseTime << "ms" << std::endl;
    std::cout << "Mean conclusion time: " << meanConclusionTime << "ms" << std::endl;
    std::cout << "Starved clients: " << starvedClients << std::endl;

    printLock.unlock();

    // --------- Cleanup ---------
    cleanupLock.lock();
    if (clientele != nullptr)
        delete clientele;
    clientele = nullptr;
    cleanupLock.unlock();

    return 0;
}

// --------- Functions ---------
int shortestCompletionFirst(
    Clientele *queue,
    int numberOfConcurrentSpots,
    std::mutex &queueLock,
    std::condition_variable_any &queueChangeCondition
) {
    std::random_device randomDevice;
    std::mt19937 randomEngine(randomDevice());
    std::uniform_int_distribution<int> randomDistribution(1, 10);

    Clientele *arrivedClients = buildEmptyList<Clientele, Client>();
    ThreadPool *threadPool = buildEmptyList<ThreadPool, Thread>();

    // We should create a thread pool with a maximum number of threads equal to numberOfConcurrentSpots
    for (int i = 0; i < numberOfConcurrentSpots; i++) {
        struct Thread *thread = new struct Thread;
        thread->thread = std::thread(
            &clientDispatcher,
            i,
            arrivedClients,
            std::ref(queueLock),
            std::ref(queueChangeCondition)
        );
        thread->thread.detach();
        thread->next = nullptr;
        thread->prev = nullptr;

        enqueue<ThreadPool, Thread>(threadPool, thread);
    }

    // We should randomly add clients to the arrived queue
    // It is important that we have cases where the client arrives before the previous one finishes
    // As well as cases where the client arrives after the previous one finishes
    // For this reason, we will randomly add clients to the arrived queue and sleep for a random duration
    Client *clientIterator = queue->head;
    while (clientIterator != nullptr) {
        // Enqueue client in arrivedClients
        // This means that the client has arrived and is waiting to be processed by the thread pool
        queueLock.lock();
        Client *nextClient = clientIterator->next;
        dequeue<Clientele, Client>(queue, clientIterator);
        enqueue<Clientele, Client>(arrivedClients, clientIterator);
        clientIterator->arrivalTime = std::chrono::system_clock::now();
        queueLock.unlock();
        printLock.lock();
        std::cout << "[SCF] Client " << (int) clientIterator->id << " arrived with duration " << clientIterator->duration.count() << std::endl;
        printLock.unlock();
        queueChangeCondition.notify_all(); // Notify all threads that a new client has arrived

        // Sleep for random duration
        int randomDuration = randomDistribution(randomEngine);
        std::this_thread::sleep_for(std::chrono::milliseconds(randomDuration));

        clientIterator = nextClient;
    }

    // We should wait for all clients to finish
    // We can do this by waiting for the arrivedClients queue to be empty
    while (arrivedClients->head != nullptr) {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(100));

    // We should stop all threads in the thread pool
    cleanupLock.lock();
    Thread *threadIterator = threadPool != nullptr ? threadPool->head : nullptr;
    while (threadIterator != nullptr) {
        threadIterator->thread.~thread();
        threadIterator = threadIterator->next;
    }
    // We should delete all created pointers
    if (threadPool != nullptr)
        delete threadPool;
    threadPool = nullptr;
    if (arrivedClients != nullptr)
        delete arrivedClients;
    arrivedClients = nullptr;
    cleanupLock.unlock();

    printLock.lock();
    std::cout << "[SCF] All clients finished" << std::endl;
    printLock.unlock();
    return OK;
}

void clientDispatcher(int id, Clientele *queue, std::mutex &queueLock, std::condition_variable_any &queueChangeCondition) {
    Client *shortestClient = nullptr;
    while (true) {
        queueLock.lock();
        queueChangeCondition.wait(queueLock);
        // If shortestClient is not null we need to return the client to the queue
        if (
            shortestClient != nullptr &&
            shortestClient->duration.count() > 0
        ) {
            enqueue<Clientele, Client>(queue, shortestClient);
        } else if (shortestClient != nullptr) {
            shortestClient = nullptr;
        }
        queueLock.unlock();

        // Get the shortest client from the queue
        printLock.lock();
        std::cout << "[DISPATCHER " << id << "] Getting shortest client" << std::endl;
        printLock.unlock();

        queueLock.lock();
        std::cout << "[DISPATCHER " << id << "] Queue size: " << queue->size << std::endl;
        Client *clientIterator = queue->head;
        shortestClient = clientIterator;
        while (clientIterator != nullptr) {
            if (
                clientIterator->duration.count() > 0 &&
                clientIterator->duration < shortestClient->duration
            ) {
                shortestClient = clientIterator;
            }

            clientIterator = clientIterator->next;
        }

        if (shortestClient == nullptr) {
            printLock.lock();
            std::cout << "[DISPATCHER " << id << "] No clients in queue" << std::endl;
            printLock.unlock();
            queueLock.unlock();
            continue;
        }

        // Remove client from queue
        dequeue<Clientele, Client>(queue, shortestClient);
        queueLock.unlock();

        // Process client
        std::thread workerThread(
            &clientWorker,
            id,
            shortestClient,
            std::ref(queueLock),
            std::ref(queueChangeCondition)
        );
        workerThread.detach();
    }
}

void clientWorker(int id, Client *client, std::mutex &queueLock, std::condition_variable_any &queueChangeCondition) {
    if (client == nullptr) {
        return;
    }

    if (client->duration.count() <= 0) {
        printLock.lock();
        std::cout << "[WORKER " << id << "] Client " << (int) client->id << " already finished" << std::endl;
        printLock.unlock();
        queueChangeCondition.notify_all();
        return;
    }
    printLock.lock();
    std::cout << "[WORKER " << id << "] Client " << (int) client->id << " started with duration " << client->duration.count() << std::endl;
    printLock.unlock();

    std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();

    queueLock.lock();
    client->responseTime = client->responseTime == std::chrono::time_point<std::chrono::system_clock>() ? std::chrono::system_clock::now() : client->responseTime;
    queueChangeCondition.wait_for(queueLock, client->duration > std::chrono::milliseconds(0) ? client->duration : std::chrono::milliseconds(1));
    std::chrono::time_point<std::chrono::system_clock> end = std::chrono::system_clock::now();
    client->duration -= std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    queueLock.unlock();

    if (client->duration.count() > 0) {
        printLock.lock();
        std::cout << "[WORKER " << id << "] Client " << (int) client->id << " interrupted" << std::endl;
        std::cout << "[WORKER " << id << "] Client " << (int) client->id << " remaining time: " << client->duration.count() << std::endl;
        printLock.unlock();
    } else {
        printLock.lock();
        std::cout << "[WORKER " << id << "] Client " << (int) client->id << " finished" << std::endl;
        client->conclusionTime = std::chrono::system_clock::now();
        finishedClients.push_back(client);
        queueChangeCondition.notify_all();
        printLock.unlock();
    }
}

template <typename listType, typename itemType>
listType *buildEmptyList() {
    listType *list = new listType;
    list->head = nullptr;
    list->tail = nullptr;
    list->size = 0;

    return list;
}

template <typename listType, typename itemType>
int enqueue(listType *list, itemType *item) {
    if (list->head == nullptr) {
        list->head = item;
        list->tail = item;
    } else {
        list->tail->next = item;
        item->prev = list->tail;
        list->tail = item;
    }

    list->size++;

    return OK;
}

template <typename listType, typename itemType>
int dequeue(listType *list, itemType *item) {
    if (list->head == nullptr) {
        return 1;
    }

    if (list->head == item) {
        list->head = item->next;
    }

    if (list->tail == item) {
        list->tail = item->prev;
    }

    if (item->prev != nullptr) {
        item->prev->next = item->next;
    }

    if (item->next != nullptr) {
        item->next->prev = item->prev;
    }

    item->next = nullptr;
    item->prev = nullptr;
    list->size--;

    return OK;
}