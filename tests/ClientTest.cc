#include "TicketOs/domain/Client.h"
#include "TicketOs/domain/Types.h"
#include <gtest/gtest.h>
#include <map>

TEST(ClientTest, can_be_created)
{
    TicketOs::Domain::Client client = TicketOs::Domain::Client::create(TicketOs::Domain::IntegerId::fromInteger(1), 0, std::chrono::milliseconds(0));
    ASSERT_EQ(client.getId().toInteger(), 1);
    ASSERT_EQ(client.getStars(), 0);
}

TEST(ClientTest, can_be_created_from_persistence)
{
    std::map<std::string, std::string> data;
    data["id"] = "1";
    data["stars"] = "0";
    data["usageTime"] = "0";

    std::vector<std::map<std::string, std::string>> clientHistoriesData = {};

    TicketOs::Domain::Client client = TicketOs::Domain::Client::fromPersistence(data, clientHistoriesData);
    ASSERT_EQ(client.getId().toInteger(), 1);
    ASSERT_EQ(client.getStars(), 0);
}

TEST(ClientTest, cannot_be_created_from_persistence_without_id)
{
    std::map<std::string, std::string> data;
    data["stars"] = "0";

    std::vector<std::map<std::string, std::string>> clientHistoriesData = {};

    ASSERT_THROW(TicketOs::Domain::Client::fromPersistence(data, clientHistoriesData), std::invalid_argument);
}

TEST(ClientTest, cannot_be_created_from_persistence_without_stars)
{
    std::map<std::string, std::string> data;
    data["id"] = "1";

    std::vector<std::map<std::string, std::string>> clientHistoriesData = {};

    ASSERT_THROW(TicketOs::Domain::Client::fromPersistence(data, clientHistoriesData), std::invalid_argument);
}

TEST(ClientTest, can_get_id)
{
    TicketOs::Domain::Client client = TicketOs::Domain::Client::create(TicketOs::Domain::IntegerId::fromInteger(1), 0, std::chrono::milliseconds(0));
    ASSERT_EQ(client.getId().toInteger(), 1);
}

TEST(ClientTest, can_get_stars)
{
    TicketOs::Domain::Client client = TicketOs::Domain::Client::create(TicketOs::Domain::IntegerId::fromInteger(1), 0, std::chrono::milliseconds(0));
    ASSERT_EQ(client.getStars(), 0);
}

TEST(ClientTest, can_be_converted_to_data)
{
    TicketOs::Domain::Client client = TicketOs::Domain::Client::create(TicketOs::Domain::IntegerId::fromInteger(1), 0, std::chrono::milliseconds(0));
    std::map<std::string, std::string> data = client.toData();
    ASSERT_EQ(data["id"], "1");
    ASSERT_EQ(data["stars"], "0");
}

TEST(ClientTest, operator_equal_returns_true_if_stars_are_equal)
{
    TicketOs::Domain::Client client1 = TicketOs::Domain::Client::create(TicketOs::Domain::IntegerId::fromInteger(1), 0, std::chrono::milliseconds(0));
    TicketOs::Domain::Client client2 = TicketOs::Domain::Client::create(TicketOs::Domain::IntegerId::fromInteger(2), 0, std::chrono::milliseconds(0));
    ASSERT_TRUE(client1 == client2);
}

TEST(ClientTest, operator_equal_returns_false_if_stars_are_not_equal)
{
    TicketOs::Domain::Client client1 = TicketOs::Domain::Client::create(TicketOs::Domain::IntegerId::fromInteger(1), 0, std::chrono::milliseconds(0));
    TicketOs::Domain::Client client2 = TicketOs::Domain::Client::create(TicketOs::Domain::IntegerId::fromInteger(2), 1, std::chrono::milliseconds(0));
    ASSERT_FALSE(client1 == client2);
}

TEST(ClientTest, operator_greater_than_returns_true_if_stars_are_greater)
{
    TicketOs::Domain::Client client1 = TicketOs::Domain::Client::create(TicketOs::Domain::IntegerId::fromInteger(1), 1, std::chrono::milliseconds(0));
    TicketOs::Domain::Client client2 = TicketOs::Domain::Client::create(TicketOs::Domain::IntegerId::fromInteger(2), 0, std::chrono::milliseconds(0));
    ASSERT_TRUE(client1 > client2);
}

TEST(ClientTest, operator_greater_than_returns_false_if_stars_are_not_greater)
{
    TicketOs::Domain::Client client1 = TicketOs::Domain::Client::create(TicketOs::Domain::IntegerId::fromInteger(1), 0, std::chrono::milliseconds(0));
    TicketOs::Domain::Client client2 = TicketOs::Domain::Client::create(TicketOs::Domain::IntegerId::fromInteger(2), 1, std::chrono::milliseconds(0));
    ASSERT_FALSE(client1 > client2);
}

TEST(ClientTest, operator_less_than_returns_true_if_stars_are_less)
{
    TicketOs::Domain::Client client1 = TicketOs::Domain::Client::create(TicketOs::Domain::IntegerId::fromInteger(1), 0, std::chrono::milliseconds(0));
    TicketOs::Domain::Client client2 = TicketOs::Domain::Client::create(TicketOs::Domain::IntegerId::fromInteger(2), 1, std::chrono::milliseconds(0));
    ASSERT_TRUE(client1 < client2);
}

TEST(ClientTest, operator_less_than_returns_false_if_stars_are_not_less)
{
    TicketOs::Domain::Client client1 = TicketOs::Domain::Client::create(TicketOs::Domain::IntegerId::fromInteger(1), 1, std::chrono::milliseconds(0));
    TicketOs::Domain::Client client2 = TicketOs::Domain::Client::create(TicketOs::Domain::IntegerId::fromInteger(2), 0, std::chrono::milliseconds(0));
    ASSERT_FALSE(client1 < client2);
}

