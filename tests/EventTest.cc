#include "TicketOs/domain/Event.h"

#include "TicketOs/domain/IntegerId.h"
#include "TicketOs/domain/DateTime.h"

#include <gtest/gtest.h>
#include <vector>
#include <map>
#include <string>

TEST(EventTest, can_be_created) {
    TicketOs::Domain::Event event = TicketOs::Domain::Event::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::DateTime::fromISO("2021-01-01T00:00:00"),
        std::vector<TicketOs::Domain::Ticket>(),
        TicketOs::Domain::IntegerId::fromInteger(2),
        "home",
        "away"
    );

    ASSERT_EQ(event.getId().toInteger(), 1);
    ASSERT_EQ(event.getDateTime().toISO(), "2021-01-01T00:00:00");
    ASSERT_EQ(event.getAvailableTickets().size(), 0);
    ASSERT_EQ(event.getSchedulerId().toInteger(), 2);
    ASSERT_EQ(event.getHomeTeam(), "home");
    ASSERT_EQ(event.getAwayTeam(), "away");
}

TEST(EventTest, can_be_created_from_persistence) {
    std::map<std::string, std::string> data;
    data["id"] = "1";
    data["dateTime"] = "2021-01-01T00:00:00";
    data["schedulerId"] = "2";
    data["homeTeam"] = "home";
    data["awayTeam"] = "away";

    std::vector<std::map<std::string, std::string>> ticketsData;

    TicketOs::Domain::Event event = TicketOs::Domain::Event::fromPersistence(data, ticketsData);

    ASSERT_EQ(event.getId().toInteger(), 1);
    ASSERT_EQ(event.getDateTime().toISO(), "2021-01-01T00:00:00");
    ASSERT_EQ(event.getAvailableTickets().size(), 0);
    ASSERT_EQ(event.getSchedulerId().toInteger(), 2);
    ASSERT_EQ(event.getHomeTeam(), "home");
    ASSERT_EQ(event.getAwayTeam(), "away");
}

TEST(EventTest, cannot_be_created_from_persistence_without_id) {
    std::map<std::string, std::string> data;
    data["dateTime"] = "2021-01-01T00:00:00";
    data["schedulerId"] = "2";
    data["homeTeam"] = "home";
    data["awayTeam"] = "away";

    std::vector<std::map<std::string, std::string>> ticketsData;

    ASSERT_THROW(TicketOs::Domain::Event::fromPersistence(data, ticketsData), std::invalid_argument);
}

TEST(EventTest, cannot_be_created_from_persistence_without_dateTime) {
    std::map<std::string, std::string> data;
    data["id"] = "1";
    data["schedulerId"] = "2";
    data["homeTeam"] = "home";
    data["awayTeam"] = "away";

    std::vector<std::map<std::string, std::string>> ticketsData;

    ASSERT_THROW(TicketOs::Domain::Event::fromPersistence(data, ticketsData), std::invalid_argument);
}

TEST(EventTest, cannot_be_created_from_persistence_without_schedulerId) {
    std::map<std::string, std::string> data;
    data["id"] = "1";
    data["dateTime"] = "2021-01-01T00:00:00";
    data["homeTeam"] = "home";
    data["awayTeam"] = "away";

    std::vector<std::map<std::string, std::string>> ticketsData;

    ASSERT_THROW(TicketOs::Domain::Event::fromPersistence(data, ticketsData), std::invalid_argument);
}

TEST(EventTest, cannot_be_created_from_persistence_without_homeTeam) {
    std::map<std::string, std::string> data;
    data["id"] = "1";
    data["dateTime"] = "2021-01-01T00:00:00";
    data["schedulerId"] = "2";
    data["awayTeam"] = "away";

    std::vector<std::map<std::string, std::string>> ticketsData;

    ASSERT_THROW(TicketOs::Domain::Event::fromPersistence(data, ticketsData), std::invalid_argument);
}

TEST(EventTest, cannot_be_created_from_persistence_without_awayTeam) {
    std::map<std::string, std::string> data;
    data["id"] = "1";
    data["dateTime"] = "2021-01-01T00:00:00";
    data["schedulerId"] = "2";
    data["homeTeam"] = "home";

    std::vector<std::map<std::string, std::string>> ticketsData;

    ASSERT_THROW(TicketOs::Domain::Event::fromPersistence(data, ticketsData), std::invalid_argument);
}

TEST(EventTest, can_be_created_with_standard_capacity) {
    TicketOs::Domain::Event event = TicketOs::Domain::Event::createStandard(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::DateTime::fromISO("2021-01-01T00:00:00"),
        TicketOs::Domain::IntegerId::fromInteger(2),
        "home",
        "away"
    );

    ASSERT_EQ(event.getId().toInteger(), 1);
    ASSERT_EQ(event.getDateTime().toISO(), "2021-01-01T00:00:00");
    ASSERT_EQ(event.getAvailableTickets().size(), STANDARD_CAPACITY);
    ASSERT_EQ(event.getSchedulerId().toInteger(), 2);
    ASSERT_EQ(event.getHomeTeam(), "home");
    ASSERT_EQ(event.getAwayTeam(), "away");
}

TEST(EventTest, can_be_created_with_low_capacity) {
    TicketOs::Domain::Event event = TicketOs::Domain::Event::createLowCapacity(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::DateTime::fromISO("2021-01-01T00:00:00"),
        TicketOs::Domain::IntegerId::fromInteger(2),
        "home",
        "away"
    );

    ASSERT_EQ(event.getId().toInteger(), 1);
    ASSERT_EQ(event.getDateTime().toISO(), "2021-01-01T00:00:00");
    ASSERT_EQ(event.getAvailableTickets().size(), LOW_CAPACITY);
    ASSERT_EQ(event.getSchedulerId().toInteger(), 2);
    ASSERT_EQ(event.getHomeTeam(), "home");
    ASSERT_EQ(event.getAwayTeam(), "away");
}

TEST(EventTest, can_be_created_with_high_capacity) {
    TicketOs::Domain::Event event = TicketOs::Domain::Event::createHighCapacity(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::DateTime::fromISO("2021-01-01T00:00:00"),
        TicketOs::Domain::IntegerId::fromInteger(2),
        "home",
        "away"
    );

    ASSERT_EQ(event.getId().toInteger(), 1);
    ASSERT_EQ(event.getDateTime().toISO(), "2021-01-01T00:00:00");
    ASSERT_EQ(event.getAvailableTickets().size(), HIGH_CAPACITY);
    ASSERT_EQ(event.getSchedulerId().toInteger(), 2);
    ASSERT_EQ(event.getHomeTeam(), "home");
    ASSERT_EQ(event.getAwayTeam(), "away");
}

TEST(EventTest, can_get_id) {
    TicketOs::Domain::Event event = TicketOs::Domain::Event::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::DateTime::fromISO("2021-01-01T00:00:00"),
        std::vector<TicketOs::Domain::Ticket>(),
        TicketOs::Domain::IntegerId::fromInteger(2),
        "home",
        "away"
    );

    ASSERT_EQ(event.getId().toInteger(), 1);
}

TEST(EventTest, can_be_converted_to_data) {
    TicketOs::Domain::Event event = TicketOs::Domain::Event::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::DateTime::fromISO("2021-01-01T00:00:00"),
        std::vector<TicketOs::Domain::Ticket>(),
        TicketOs::Domain::IntegerId::fromInteger(2),
        "home",
        "away"
    );

    std::map<std::string, std::string> data = event.toData();

    ASSERT_EQ(data["id"], "1");
    ASSERT_EQ(data["dateTime"], "2021-01-01T00:00:00");
    ASSERT_EQ(data["schedulerId"], "2");
    ASSERT_EQ(data["homeTeam"], "home");
    ASSERT_EQ(data["awayTeam"], "away");
}
