#include "TicketOs/infrastructure/binary/BinaryIdRepository.h"

#include <gtest/gtest.h>
#include <string>

TEST(BinaryIdRepositoryTest, can_retrieve_next_id_incremented) {
    TicketOs::Infrastructure::BinaryIdRepository repository("id_repository.binary");

    ASSERT_EQ(
        repository.nextId().toInteger(),
        1
    );
    ASSERT_EQ(
        repository.nextId().toInteger(),
        2
    );
    ASSERT_EQ(
        repository.nextId().toInteger(),
        3
    );
}