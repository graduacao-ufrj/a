#include "TicketOs/domain/TicketService.h"

#include <gtest/gtest.h>

TEST(TicketServiceTest, can_build_amount_of_tickets)
{
    TicketOs::Domain::IntegerId eventId = TicketOs::Domain::IntegerId::fromInteger(1);
    std::vector<TicketOs::Domain::Ticket> tickets = TicketOs::Service::TicketService::buildAmountOfTickets(eventId, 100, "id_sequence");
    ASSERT_EQ(tickets.size(), 100);
}

TEST(TicketServiceTest, it_builds_tickets_with_correct_ratios)
{
    TicketOs::Domain::IntegerId eventId = TicketOs::Domain::IntegerId::fromInteger(1);
    std::vector<TicketOs::Domain::Ticket> tickets = TicketOs::Service::TicketService::buildAmountOfTickets(eventId, 100, "id_sequence");

    unsigned int honorTribuneTickets = 0;
    unsigned int vipAreaTickets = 0;
    unsigned int associatesAreaTickets = 0;
    unsigned int boxesTickets = 0;
    unsigned int nonAssociatesAreaTickets = 0;
    unsigned int visitorAreaTickets = 0;
    unsigned int pressAreaTickets = 0;

    for (auto ticket : tickets) {
        switch (ticket.getSector()) {
            case TicketOs::Types::sectorType::honorTribune:
                honorTribuneTickets++;
                break;
            case TicketOs::Types::sectorType::vipArea:
                vipAreaTickets++;
                break;
            case TicketOs::Types::sectorType::associatesArea:
                associatesAreaTickets++;
                break;
            case TicketOs::Types::sectorType::boxes:
                boxesTickets++;
                break;
            case TicketOs::Types::sectorType::nonAssociatesArea:
                nonAssociatesAreaTickets++;
                break;
            case TicketOs::Types::sectorType::visitorArea:
                visitorAreaTickets++;
                break;
            case TicketOs::Types::sectorType::pressArea:
                pressAreaTickets++;
                break;
        }
    }

    ASSERT_EQ(honorTribuneTickets, 5);
    ASSERT_EQ(vipAreaTickets, 5);
    ASSERT_EQ(associatesAreaTickets, 20);
    ASSERT_EQ(boxesTickets, 10);
    ASSERT_EQ(nonAssociatesAreaTickets, 50);
    ASSERT_EQ(visitorAreaTickets, 8);
    ASSERT_EQ(pressAreaTickets, 2);
}