#include "TicketOs/domain/ClientHistory.h"
#include "TicketOs/domain/Types.h"
#include "TicketOs/domain/IntegerId.h"
#include "TicketOs/domain/DateTime.h"

#include <ctime>
#include <map>
#include <gtest/gtest.h>

TEST(ClientHistoryTest, can_be_created)
{
    TicketOs::Domain::ClientHistory clientHistory = TicketOs::Domain::ClientHistory::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::IntegerId::fromInteger(1)
    );

    ASSERT_EQ(clientHistory.getId().toInteger(), 1);
    ASSERT_EQ(clientHistory.getClientId().toInteger(), 1);
    ASSERT_EQ(clientHistory.getEventId().toInteger(), 1);
}

TEST(ClientHistoryTest, can_be_created_from_persistence)
{
    std::map<std::string, std::string> data;
    data["id"] = "1";
    data["clientId"] = "1";
    data["arrivalTime"] = "2024-02-20T00:00:00";
    data["responseTime"] = "2024-02-20T01:00:00";
    data["conclusionTime"] = "2024-02-20T02:00:00";
    data["eventId"] = "1";
    data["successful"] = "1";
    TicketOs::Domain::ClientHistory clientHistory = TicketOs::Domain::ClientHistory::fromPersistence(data);

    ASSERT_EQ(clientHistory.getId().toInteger(), 1);
    ASSERT_EQ(clientHistory.getClientId().toInteger(), 1);
    ASSERT_EQ(clientHistory.getEventId().toInteger(), 1);
    ASSERT_TRUE(clientHistory.wasSuccessful());
}

TEST(ClientHistoryTest, cannot_be_created_from_persistence_without_id)
{
    std::map<std::string, std::string> data;
    data["clientId"] = "1";
    data["arrivalTime"] = "2024-02-20T00:00:00";
    data["responseTime"] = "2024-02-20T01:00:00";
    data["conclusionTime"] = "2024-02-20T02:00:00";
    data["eventId"] = "1";
    data["successful"] = "1";
    ASSERT_THROW(TicketOs::Domain::ClientHistory::fromPersistence(data), std::invalid_argument);
}

TEST(ClientHistoryTest, cannot_be_created_from_persistence_without_clientId)
{
    std::map<std::string, std::string> data;
    data["id"] = "1";
    data["arrivalTime"] = "2024-02-20T00:00:00";
    data["responseTime"] = "2024-02-20T01:00:00";
    data["conclusionTime"] = "2024-02-20T02:00:00";
    data["eventId"] = "1";
    data["successful"] = "1";
    ASSERT_THROW(TicketOs::Domain::ClientHistory::fromPersistence(data), std::invalid_argument);
}

TEST(ClientHistoryTest, cannot_be_created_from_persistence_without_arrivalTime)
{
    std::map<std::string, std::string> data;
    data["id"] = "1";
    data["clientId"] = "1";
    data["responseTime"] = "2024-02-20T01:00:00";
    data["conclusionTime"] = "2024-02-20T02:00:00";
    data["eventId"] = "1";
    data["successful"] = "1";
    ASSERT_THROW(TicketOs::Domain::ClientHistory::fromPersistence(data), std::invalid_argument);
}

TEST(ClientHistoryTest, cannot_be_created_from_persistence_without_responseTime)
{
    std::map<std::string, std::string> data;
    data["id"] = "1";
    data["clientId"] = "1";
    data["arrivalTime"] = "2024-02-20T00:00:00";
    data["conclusionTime"] = "2024-02-20T02:00:00";
    data["eventId"] = "1";
    data["successful"] = "1";
    ASSERT_THROW(TicketOs::Domain::ClientHistory::fromPersistence(data), std::invalid_argument);
}

TEST(ClientHistoryTest, cannot_be_created_from_persistence_without_conclusionTime)
{
    std::map<std::string, std::string> data;
    data["id"] = "1";
    data["clientId"] = "1";
    data["arrivalTime"] = "2024-02-20T00:00:00";
    data["responseTime"] = "2024-02-20T01:00:00";
    data["eventId"] = "1";
    data["successful"] = "1";
    ASSERT_THROW(TicketOs::Domain::ClientHistory::fromPersistence(data), std::invalid_argument);
}

TEST(ClientHistoryTest, cannot_be_created_from_persistence_without_eventId)
{
    std::map<std::string, std::string> data;
    data["id"] = "1";
    data["clientId"] = "1";
    data["arrivalTime"] = "2024-02-20T00:00:00";
    data["responseTime"] = "2024-02-20T01:00:00";
    data["conclusionTime"] = "2024-02-20T02:00:00";
    data["successful"] = "1";
    ASSERT_THROW(TicketOs::Domain::ClientHistory::fromPersistence(data), std::invalid_argument);
}

TEST(ClientHistoryTest, cannot_be_created_from_persistence_without_successful)
{
    std::map<std::string, std::string> data;
    data["id"] = "1";
    data["clientId"] = "1";
    data["arrivalTime"] = "2024-02-20T00:00:00";
    data["responseTime"] = "2024-02-20T01:00:00";
    data["conclusionTime"] = "2024-02-20T02:00:00";
    data["eventId"] = "1";
    ASSERT_THROW(TicketOs::Domain::ClientHistory::fromPersistence(data), std::invalid_argument);
}

TEST(ClientHistoryTest, can_get_id)
{
    TicketOs::Domain::ClientHistory clientHistory = TicketOs::Domain::ClientHistory::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::IntegerId::fromInteger(1)
    );

    ASSERT_EQ(clientHistory.getId().toInteger(), 1);
}

TEST(ClientHistoryTest, can_get_clientId)
{
    TicketOs::Domain::ClientHistory clientHistory = TicketOs::Domain::ClientHistory::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::IntegerId::fromInteger(1)
    );

    ASSERT_EQ(clientHistory.getClientId().toInteger(), 1);
}

TEST(ClientHistoryTest, can_get_arrivalTime)
{
    std::map<std::string, std::string> data;
    data["id"] = "1";
    data["clientId"] = "1";
    data["arrivalTime"] = "2024-02-20T00:00:00";
    data["responseTime"] = "2024-02-20T01:00:00";
    data["conclusionTime"] = "2024-02-20T02:00:00";
    data["eventId"] = "1";
    data["successful"] = "1";
    TicketOs::Domain::ClientHistory clientHistory = TicketOs::Domain::ClientHistory::fromPersistence(data);

    TicketOs::Domain::DateTime arrivalTime = clientHistory.getArrivalTime();

    ASSERT_EQ(arrivalTime.toISO(), "2024-02-20T00:00:00");
}

TEST(ClientHistoryTest, can_get_responseTime)
{
    std::map<std::string, std::string> data;
    data["id"] = "1";
    data["clientId"] = "1";
    data["arrivalTime"] = "2024-02-20T00:00:00";
    data["responseTime"] = "2024-02-20T01:00:00";
    data["conclusionTime"] = "2024-02-20T02:00:00";
    data["eventId"] = "1";
    data["successful"] = "1";
    TicketOs::Domain::ClientHistory clientHistory = TicketOs::Domain::ClientHistory::fromPersistence(data);

    TicketOs::Domain::DateTime *responseTime = clientHistory.getResponseTime();

    ASSERT_EQ(responseTime->toISO(), "2024-02-20T01:00:00");
}

TEST(ClientHistoryTest, can_get_conclusionTime)
{
    std::map<std::string, std::string> data;
    data["id"] = "1";
    data["clientId"] = "1";
    data["arrivalTime"] = "2024-02-20T00:00:00";
    data["responseTime"] = "2024-02-20T01:00:00";
    data["conclusionTime"] = "2024-02-20T02:00:00";
    data["eventId"] = "1";
    data["successful"] = "1";
    TicketOs::Domain::ClientHistory clientHistory = TicketOs::Domain::ClientHistory::fromPersistence(data);

    TicketOs::Domain::DateTime *conclusionTime = clientHistory.getConclusionTime();

    ASSERT_EQ(conclusionTime->toISO(), "2024-02-20T02:00:00");
}

TEST(ClientHistoryTest, can_set_responseTime)
{
    TicketOs::Domain::ClientHistory clientHistory = TicketOs::Domain::ClientHistory::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::IntegerId::fromInteger(1)
    );

    TicketOs::Domain::DateTime now = TicketOs::Domain::DateTime::now();

    ASSERT_EQ(clientHistory.setResponseTime(), TicketOs::Types::errorType::ok);
    ASSERT_EQ(clientHistory.getResponseTime()->toISO(), now.toISO());
}

TEST(ClientHistoryTest, cannot_set_responseTime_if_already_responded)
{
    TicketOs::Domain::ClientHistory clientHistory = TicketOs::Domain::ClientHistory::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::IntegerId::fromInteger(1)
    );

    clientHistory.setResponseTime();

    ASSERT_EQ(clientHistory.setResponseTime(), TicketOs::Types::errorType::clientHistoryAlreadyResponded);
}

TEST(ClientHistoryTest, can_set_conclusionTime)
{
    TicketOs::Domain::ClientHistory clientHistory = TicketOs::Domain::ClientHistory::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::IntegerId::fromInteger(1)
    );

    clientHistory.setResponseTime();
    TicketOs::Domain::DateTime now = TicketOs::Domain::DateTime::now();

    ASSERT_EQ(clientHistory.setConclusionTime(), TicketOs::Types::errorType::ok);
    ASSERT_EQ(clientHistory.getConclusionTime()->toISO(), now.toISO());
}

TEST(ClientHistoryTest, cannot_set_conclusionTime_if_already_concluded)
{
    TicketOs::Domain::ClientHistory clientHistory = TicketOs::Domain::ClientHistory::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::IntegerId::fromInteger(1)
    );

    clientHistory.setResponseTime();
    clientHistory.setConclusionTime();

    ASSERT_EQ(clientHistory.setConclusionTime(), TicketOs::Types::errorType::clientHistoryAlreadyConcluded);
}

TEST(ClientHistoryTest, cannot_set_conclusionTime_if_not_responded)
{
    TicketOs::Domain::ClientHistory clientHistory = TicketOs::Domain::ClientHistory::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::IntegerId::fromInteger(1)
    );

    ASSERT_EQ(clientHistory.setConclusionTime(), TicketOs::Types::errorType::clientHistoryNotResponded);
}

TEST(ClientHistoryTest, can_get_eventId)
{
    TicketOs::Domain::ClientHistory clientHistory = TicketOs::Domain::ClientHistory::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::IntegerId::fromInteger(1)
    );

    ASSERT_EQ(clientHistory.getEventId().toInteger(), 1);
}

TEST(ClientHistoryTest, can_know_if_responded)
{
    TicketOs::Domain::ClientHistory clientHistory = TicketOs::Domain::ClientHistory::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::IntegerId::fromInteger(1)
    );

    ASSERT_FALSE(clientHistory.isResponded());

    clientHistory.setResponseTime();

    ASSERT_TRUE(clientHistory.isResponded());
}

TEST(ClientHistoryTest, can_know_if_concluded)
{
    TicketOs::Domain::ClientHistory clientHistory = TicketOs::Domain::ClientHistory::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Domain::IntegerId::fromInteger(1)
    );

    ASSERT_FALSE(clientHistory.isConcluded());

    clientHistory.setResponseTime();
    clientHistory.setConclusionTime();

    ASSERT_TRUE(clientHistory.isConcluded());
}

TEST(ClientHistoryTest, can_convert_to_data)
{
    std::map<std::string, std::string> expectedData;
    expectedData["id"] = "1";
    expectedData["clientId"] = "1";
    expectedData["arrivalTime"] = "2024-02-20T00:00:00";
    expectedData["responseTime"] = "2024-02-20T01:00:00";
    expectedData["conclusionTime"] = "2024-02-20T02:00:00";
    expectedData["eventId"] = "1";
    expectedData["successful"] = "1";

    TicketOs::Domain::ClientHistory clientHistory = TicketOs::Domain::ClientHistory::fromPersistence(expectedData);

    std::map<std::string, std::string> data = clientHistory.toData();

    ASSERT_EQ(data["id"], expectedData["id"]);
    ASSERT_EQ(data["clientId"], expectedData["clientId"]);
    ASSERT_EQ(data["arrivalTime"], expectedData["arrivalTime"]);
    ASSERT_EQ(data["responseTime"], expectedData["responseTime"]);
    ASSERT_EQ(data["conclusionTime"], expectedData["conclusionTime"]);
    ASSERT_EQ(data["eventId"], expectedData["eventId"]);
    ASSERT_EQ(data["successful"], expectedData["successful"]);
}