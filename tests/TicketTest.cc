#include "TicketOs/domain/Ticket.h"
#include "TicketOs/domain/IntegerId.h"
#include "TicketOs/domain/Types.h"

#include <gtest/gtest.h>
#include <map>

/**
 * sectorType = {
 *   honorTribune,
 *   vipArea,
 *   associatesArea,
 *   boxes,
 *   nonAssociatesArea,
 *   visitor,
 *   press,
 *   sectorMissing
 * }
*/

TEST(TicketTest, can_be_created)
{
    TicketOs::Domain::IntegerId client = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId eventId = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::Ticket ticket = TicketOs::Domain::Ticket::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Types::sectorType::vipArea,
        1,
        1,
        eventId,
        &client
    );
    ASSERT_EQ(ticket.getId().toInteger(), 1);
    ASSERT_EQ(ticket.getSector(), TicketOs::Types::sectorType::vipArea);
    ASSERT_EQ(ticket.getRow(), 1);
    ASSERT_EQ(ticket.getSeat(), 1);
    ASSERT_EQ(ticket.getOwnerId(), &client);
}

TEST(TicketTest, can_be_created_without_owner)
{
    TicketOs::Domain::IntegerId eventId = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::Ticket ticket = TicketOs::Domain::Ticket::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Types::sectorType::vipArea,
        1,
        1,
        eventId,
        nullptr
    );
    ASSERT_EQ(ticket.getId().toInteger(), 1);
    ASSERT_EQ(ticket.getSector(), TicketOs::Types::sectorType::vipArea);
    ASSERT_EQ(ticket.getRow(), 1);
    ASSERT_EQ(ticket.getSeat(), 1);
    ASSERT_EQ(ticket.getOwnerId(), nullptr);
}

TEST(TicketTest, can_be_created_from_persistence)
{
    std::map<std::string, std::string> data;
    data["id"] = "1";
    data["sector"] = std::to_string(TicketOs::Types::sectorType::vipArea);
    data["row"] = "3";
    data["seat"] = "4";
    data["ownerId"] = "5";
    data["eventId"] = "1";

    TicketOs::Domain::Ticket ticket = TicketOs::Domain::Ticket::fromPersistence(data);

    ASSERT_EQ(ticket.getId().toInteger(), 1);
    ASSERT_EQ(ticket.getSector(), TicketOs::Types::sectorType::vipArea);
    ASSERT_EQ(ticket.getRow(), 3);
    ASSERT_EQ(ticket.getSeat(), 4);
    ASSERT_EQ(ticket.getOwnerId()->toInteger(), 5);
}

TEST(TicketTest, can_be_created_from_persistence_without_owner)
{
    std::map<std::string, std::string> data;
    data["id"] = "1";
    data["sector"] = std::to_string(TicketOs::Types::sectorType::vipArea);
    data["row"] = "1";
    data["seat"] = "1";
    data["eventId"] = "1";

    TicketOs::Domain::Ticket ticket = TicketOs::Domain::Ticket::fromPersistence(data);
    ASSERT_EQ(ticket.getId().toInteger(), 1);
    ASSERT_EQ(ticket.getSector(), TicketOs::Types::sectorType::vipArea);
    ASSERT_EQ(ticket.getRow(), 1);
    ASSERT_EQ(ticket.getSeat(), 1);
    ASSERT_EQ(ticket.getOwnerId(), nullptr);
}

TEST(TicketTest, cannot_be_created_from_persistence_without_id)
{
    std::map<std::string, std::string> data;
    data["sector"] = std::to_string(TicketOs::Types::sectorType::vipArea);
    data["row"] = "1";
    data["seat"] = "1";
    data["ownerId"] = "1";
    ASSERT_THROW(TicketOs::Domain::Ticket::fromPersistence(data), std::invalid_argument);
}

TEST(TicketTest, cannot_be_created_from_persistence_without_sector)
{
    std::map<std::string, std::string> data;
    data["id"] = "1";
    data["row"] = "1";
    data["seat"] = "1";
    data["ownerId"] = "1";
    ASSERT_THROW(TicketOs::Domain::Ticket::fromPersistence(data), std::invalid_argument);
}

TEST(TicketTest, cannot_be_created_from_persistence_without_row)
{
    std::map<std::string, std::string> data;
    data["id"] = "1";
    data["sector"] = std::to_string(TicketOs::Types::sectorType::vipArea);
    data["seat"] = "1";
    data["ownerId"] = "1";
    ASSERT_THROW(TicketOs::Domain::Ticket::fromPersistence(data), std::invalid_argument);
}

TEST(TicketTest, cannot_be_created_from_persistence_without_seat)
{
    std::map<std::string, std::string> data;
    data["id"] = "1";
    data["sector"] = std::to_string(TicketOs::Types::sectorType::vipArea);
    data["row"] = "1";
    data["ownerId"] = "1";
    ASSERT_THROW(TicketOs::Domain::Ticket::fromPersistence(data), std::invalid_argument);
}

TEST(TicketTest, can_get_id)
{
    TicketOs::Domain::IntegerId client = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId eventId = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::Ticket ticket = TicketOs::Domain::Ticket::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Types::sectorType::vipArea,
        1,
        1,
        eventId,
        &client
    );
    ASSERT_EQ(ticket.getId().toInteger(), 1);
}

TEST(TicketTest, can_get_sector)
{
    TicketOs::Domain::IntegerId client = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId eventId = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::Ticket ticket = TicketOs::Domain::Ticket::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Types::sectorType::vipArea,
        1,
        1,
        eventId,
        &client
    );
    ASSERT_EQ(ticket.getSector(), TicketOs::Types::sectorType::vipArea);
}

TEST(TicketTest, can_get_row)
{
    TicketOs::Domain::IntegerId client = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId eventId = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::Ticket ticket = TicketOs::Domain::Ticket::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Types::sectorType::vipArea,
        1,
        1,
        eventId,
        &client
    );
    ASSERT_EQ(ticket.getRow(), 1);
}

TEST(TicketTest, can_get_seat)
{
    TicketOs::Domain::IntegerId client = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId eventId = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::Ticket ticket = TicketOs::Domain::Ticket::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Types::sectorType::vipArea,
        1,
        1,
        eventId,
        &client
    );
    ASSERT_EQ(ticket.getSeat(), 1);
}

TEST(TicketTest, can_get_owner)
{
    TicketOs::Domain::IntegerId client = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId eventId = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::Ticket ticket = TicketOs::Domain::Ticket::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Types::sectorType::vipArea,
        1,
        1,
        eventId,
        &client
    );
    ASSERT_EQ(ticket.getOwnerId(), &client);
}

TEST(TicketTest, can_get_null_owner)
{
    TicketOs::Domain::IntegerId eventId = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::Ticket ticket = TicketOs::Domain::Ticket::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Types::sectorType::vipArea,
        1,
        1,
        eventId,
        nullptr
    );
    ASSERT_EQ(ticket.getOwnerId(), nullptr);
}

TEST(TicketTest, is_available_returns_true_if_owner_is_null)
{
    TicketOs::Domain::IntegerId eventId = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::Ticket ticket = TicketOs::Domain::Ticket::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Types::sectorType::vipArea,
        1,
        1,
        eventId,
        nullptr
    );
    ASSERT_TRUE(ticket.isAvailable());
}

TEST(TicketTest, is_available_returns_false_if_owner_is_not_null)
{
    TicketOs::Domain::IntegerId client = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId eventId = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::Ticket ticket = TicketOs::Domain::Ticket::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Types::sectorType::vipArea,
        1,
        1,
        eventId,
        &client
    );
    ASSERT_FALSE(ticket.isAvailable());
}

TEST(TicketTest, can_reserve_ticket)
{
    TicketOs::Domain::IntegerId client = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId eventId = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::Ticket ticket = TicketOs::Domain::Ticket::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Types::sectorType::vipArea,
        1,
        1,
        eventId,
        nullptr
    );
    ASSERT_EQ(ticket.reserve(&client), TicketOs::Types::errorType::ok);
    ASSERT_EQ(ticket.getOwnerId(), &client);
}

TEST(TicketTest, cannot_reserve_ticket_if_already_reserved)
{
    TicketOs::Domain::IntegerId client1 = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId client2 = TicketOs::Domain::IntegerId::fromInteger(2);
    TicketOs::Domain::IntegerId eventId = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::Ticket ticket = TicketOs::Domain::Ticket::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Types::sectorType::vipArea,
        1,
        1,
        eventId,
        &client1
    );
    ASSERT_EQ(ticket.reserve(&client2), TicketOs::Types::errorType::ticketAlreadyReserved);
    ASSERT_EQ(ticket.getOwnerId(), &client1);
}

TEST(TicketTest, cannot_reserve_ticket_if_client_is_null)
{
    TicketOs::Domain::IntegerId eventId = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::Ticket ticket = TicketOs::Domain::Ticket::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Types::sectorType::vipArea,
        1,
        1,
        eventId,
        nullptr
    );
    ASSERT_EQ(ticket.reserve(nullptr), TicketOs::Types::errorType::ticketClientNotValid);
    ASSERT_EQ(ticket.getOwnerId(), nullptr);
}

TEST(TicketTest, can_release_ticket)
{
    TicketOs::Domain::IntegerId client = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId eventId = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::Ticket ticket = TicketOs::Domain::Ticket::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Types::sectorType::vipArea,
        1,
        1,
        eventId,
        &client
    );
    ASSERT_EQ(ticket.release(), TicketOs::Types::errorType::ok);
    ASSERT_EQ(ticket.getOwnerId(), nullptr);
}

TEST(TicketTest, cannot_release_ticket_if_not_reserved)
{
    TicketOs::Domain::IntegerId eventId = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::Ticket ticket = TicketOs::Domain::Ticket::create(
        TicketOs::Domain::IntegerId::fromInteger(1),
        TicketOs::Types::sectorType::vipArea,
        1,
        1,
        eventId,
        nullptr
    );
    ASSERT_EQ(ticket.release(), TicketOs::Types::errorType::ticketNotReserved);
    ASSERT_EQ(ticket.getOwnerId(), nullptr);
}
