#include "TicketOs/domain/IntegerId.h"
#include <gtest/gtest.h>

TEST(IntegerIdTest, can_be_created_from_integer)
{
    TicketOs::Domain::IntegerId id = TicketOs::Domain::IntegerId::fromInteger(1);
    ASSERT_EQ(id.toInteger(), 1);
}

TEST(IntegerIdTest, can_be_converted_to_integer)
{
    TicketOs::Domain::IntegerId id = TicketOs::Domain::IntegerId::fromInteger(1);
    ASSERT_EQ(id.toInteger(), 1);
}

TEST(IntegerIdTest, operator_equals_returns_true_when_values_are_equal)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(1);
    ASSERT_TRUE(id1 == id2);
}

TEST(IntegerIdTest, operator_equals_returns_false_when_values_are_not_equal)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(2);
    ASSERT_FALSE(id1 == id2);
}

TEST(IntegerIdTest, operator_not_equals_returns_true_when_values_are_not_equal)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(2);
    ASSERT_TRUE(id1 != id2);
}

TEST(IntegerIdTest, operator_not_equals_returns_false_when_values_are_equal)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(1);
    ASSERT_FALSE(id1 != id2);
}

TEST(IntegerIdTest, operator_less_than_returns_true_when_value_is_less_than_other)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(2);
    ASSERT_TRUE(id1 < id2);
}

TEST(IntegerIdTest, operator_less_than_returns_false_when_value_is_greater_than_other)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(2);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(1);
    ASSERT_FALSE(id1 < id2);
}

TEST(IntegerIdTest, operator_less_than_returns_false_when_value_is_equal_to_other)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(1);
    ASSERT_FALSE(id1 < id2);
}

TEST(IntegerIdTest, operator_greater_than_returns_true_when_value_is_greater_than_other)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(2);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(1);
    ASSERT_TRUE(id1 > id2);
}

TEST(IntegerIdTest, operator_greater_than_returns_false_when_value_is_less_than_other)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(2);
    ASSERT_FALSE(id1 > id2);
}

TEST(IntegerIdTest, operator_greater_than_returns_false_when_value_is_equal_to_other)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(1);
    ASSERT_FALSE(id1 > id2);
}

TEST(IntegerIdTest, operator_less_than_or_equal_returns_true_when_value_is_less_than_other)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(2);
    ASSERT_TRUE(id1 <= id2);
}

TEST(IntegerIdTest, operator_less_than_or_equal_returns_true_when_value_is_equal_to_other)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(1);
    ASSERT_TRUE(id1 <= id2);
}

TEST(IntegerIdTest, operator_less_than_or_equal_returns_false_when_value_is_greater_than_other)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(2);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(1);
    ASSERT_FALSE(id1 <= id2);
}

TEST(IntegerIdTest, operator_greater_than_or_equal_returns_true_when_value_is_greater_than_other)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(2);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(1);
    ASSERT_TRUE(id1 >= id2);
}

TEST(IntegerIdTest, operator_greater_than_or_equal_returns_true_when_value_is_equal_to_other)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(1);
    ASSERT_TRUE(id1 >= id2);
}

TEST(IntegerIdTest, operator_greater_than_or_equal_returns_false_when_value_is_less_than_other)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(2);
    ASSERT_FALSE(id1 >= id2);
}

TEST(IntegerIdTest, operator_plus_returns_sum_of_values)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(2);
    TicketOs::Domain::IntegerId sum = id1 + id2;
    ASSERT_EQ(sum.toInteger(), 3);
}

TEST(IntegerIdTest, operator_minus_returns_difference_of_values)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(2);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId difference = id1 - id2;
    ASSERT_EQ(difference.toInteger(), 1);
}

TEST(IntegerIdTest, operator_multiply_returns_product_of_values)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(2);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(3);
    TicketOs::Domain::IntegerId product = id1 * id2;
    ASSERT_EQ(product.toInteger(), 6);
}

TEST(IntegerIdTest, operator_divide_returns_quotient_of_values)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(6);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(3);
    TicketOs::Domain::IntegerId quotient = id1 / id2;
    ASSERT_EQ(quotient.toInteger(), 2);
}

TEST(IntegerIdTest, operator_modulus_returns_remainder_of_values)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(6);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(4);
    TicketOs::Domain::IntegerId remainder = id1 % id2;
    ASSERT_EQ(remainder.toInteger(), 2);
}

TEST(IntegerIdTest, operator_plus_equals_returns_sum_of_values)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(2);
    id1 += id2;
    ASSERT_EQ(id1.toInteger(), 3);
}

TEST(IntegerIdTest, operator_minus_equals_returns_difference_of_values)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(2);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(1);
    id1 -= id2;
    ASSERT_EQ(id1.toInteger(), 1);
}

TEST(IntegerIdTest, operator_multiply_equals_returns_product_of_values)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(2);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(3);
    id1 *= id2;
    ASSERT_EQ(id1.toInteger(), 6);
}

TEST(IntegerIdTest, operator_divide_equals_returns_quotient_of_values)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(6);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(3);
    id1 /= id2;
    ASSERT_EQ(id1.toInteger(), 2);
}

TEST(IntegerIdTest, operator_modulus_equals_returns_remainder_of_values)
{
    TicketOs::Domain::IntegerId id1 = TicketOs::Domain::IntegerId::fromInteger(6);
    TicketOs::Domain::IntegerId id2 = TicketOs::Domain::IntegerId::fromInteger(4);
    id1 %= id2;
    ASSERT_EQ(id1.toInteger(), 2);
}

TEST(IntegerIdTest, operator_increment_returns_value_incremented_by_one)
{
    TicketOs::Domain::IntegerId id = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId incremented = ++id;
    ASSERT_EQ(incremented.toInteger(), 2);
}

TEST(IntegerIdTest, operator_increment_postfix_returns_value_incremented_by_one)
{
    TicketOs::Domain::IntegerId id = TicketOs::Domain::IntegerId::fromInteger(1);
    TicketOs::Domain::IntegerId incremented = id++;
    ASSERT_EQ(incremented.toInteger(), 1);
    ASSERT_EQ(id.toInteger(), 2);
}

TEST(IntegerIdTest, operator_decrement_returns_value_decremented_by_one)
{
    TicketOs::Domain::IntegerId id = TicketOs::Domain::IntegerId::fromInteger(2);
    TicketOs::Domain::IntegerId decremented = --id;
    ASSERT_EQ(decremented.toInteger(), 1);
}

TEST(IntegerIdTest, operator_decrement_postfix_returns_value_decremented_by_one)
{
    TicketOs::Domain::IntegerId id = TicketOs::Domain::IntegerId::fromInteger(2);
    TicketOs::Domain::IntegerId decremented = id--;
    ASSERT_EQ(decremented.toInteger(), 2);
    ASSERT_EQ(id.toInteger(), 1);
}
