#include "TicketOs/domain/DateTime.h"

#include <ctime>
#include <string>
#include <gtest/gtest.h>

TEST(DateTimeTest, can_be_created_from_iso)
{
    TicketOs::Domain::DateTime dateTime = TicketOs::Domain::DateTime::fromISO("2024-02-20T00:00:00");

    ASSERT_EQ(dateTime.toISO(), "2024-02-20T00:00:00");
}

TEST(DateTimeTest, cannot_be_created_from_invalid_iso)
{
    ASSERT_THROW(TicketOs::Domain::DateTime::fromISO("Mar 20 2024 00:00:00"), std::invalid_argument);
}

TEST(DateTimeTest, can_be_created_from_iso_with_only_date)
{
    TicketOs::Domain::DateTime dateTime = TicketOs::Domain::DateTime::fromISO("2024-02-20");

    ASSERT_EQ(dateTime.toISO(), "2024-02-20T00:00:00");
}

TEST(DateTimeTest, can_be_created_from_time_t)
{
    time_t time = 1692768000;
    TicketOs::Domain::DateTime dateTime = TicketOs::Domain::DateTime::fromTimeT(time);

    ASSERT_EQ(dateTime.toISO(), "2023-08-23T05:20:00");
}

TEST(DateTimeTest, can_be_created_from_tm)
{
    struct tm time;
    strptime("2024-02-20T00:00:00", "%Y-%m-%dT%H:%M:%S", &time);
    TicketOs::Domain::DateTime dateTime = TicketOs::Domain::DateTime::fromTm(&time);

    ASSERT_EQ(dateTime.toISO(), "2024-02-20T00:00:00");
}

TEST(DateTimeTest, can_get_current_time)
{
    TicketOs::Domain::DateTime dateTime = TicketOs::Domain::DateTime::now();

    time_t now = time(nullptr);
    struct tm *time = localtime(&now);

    ASSERT_TRUE(dateTime.toISO() == TicketOs::Domain::DateTime::fromTm(time).toISO());
}

TEST(DateTimeTest, operator_equal_returns_true_if_values_are_equal)
{
    TicketOs::Domain::DateTime dateTime1 = TicketOs::Domain::DateTime::fromISO("2024-02-20T00:00:00");
    TicketOs::Domain::DateTime dateTime2 = TicketOs::Domain::DateTime::fromISO("2024-02-20T00:00:00");

    ASSERT_TRUE(dateTime1 == dateTime2);
}

TEST(DateTimeTest, operator_equal_returns_false_if_values_are_not_equal)
{
    TicketOs::Domain::DateTime dateTime1 = TicketOs::Domain::DateTime::fromISO("2024-02-20T00:00:00");
    TicketOs::Domain::DateTime dateTime2 = TicketOs::Domain::DateTime::fromISO("2024-02-20T01:00:00");

    ASSERT_FALSE(dateTime1 == dateTime2);
}

TEST(DateTimeTest, operator_not_equal_returns_true_if_values_are_not_equal)
{
    TicketOs::Domain::DateTime dateTime1 = TicketOs::Domain::DateTime::fromISO("2024-02-20T00:00:00");
    TicketOs::Domain::DateTime dateTime2 = TicketOs::Domain::DateTime::fromISO("2024-02-20T01:00:00");

    ASSERT_TRUE(dateTime1 != dateTime2);
}

TEST(DateTimeTest, operator_not_equal_returns_false_if_values_are_equal)
{
    TicketOs::Domain::DateTime dateTime1 = TicketOs::Domain::DateTime::fromISO("2024-02-20T00:00:00");
    TicketOs::Domain::DateTime dateTime2 = TicketOs::Domain::DateTime::fromISO("2024-02-20T00:00:00");

    ASSERT_FALSE(dateTime1 != dateTime2);
}

TEST(DateTimeTest, operator_less_than_returns_true_if_value_is_less_than_other)
{
    TicketOs::Domain::DateTime dateTime1 = TicketOs::Domain::DateTime::fromISO("2024-02-20T00:00:00");
    TicketOs::Domain::DateTime dateTime2 = TicketOs::Domain::DateTime::fromISO("2024-02-20T01:00:00");

    ASSERT_TRUE(dateTime1 < dateTime2);
}

TEST(DateTimeTest, operator_less_than_returns_false_if_value_is_not_less_than_other)
{
    TicketOs::Domain::DateTime dateTime1 = TicketOs::Domain::DateTime::fromISO("2024-02-20T01:00:00");
    TicketOs::Domain::DateTime dateTime2 = TicketOs::Domain::DateTime::fromISO("2024-02-20T00:00:00");

    ASSERT_FALSE(dateTime1 < dateTime2);
}

TEST(DateTimeTest, operator_greater_than_returns_true_if_value_is_greater_than_other)
{
    TicketOs::Domain::DateTime dateTime1 = TicketOs::Domain::DateTime::fromISO("2024-02-20T01:00:00");
    TicketOs::Domain::DateTime dateTime2 = TicketOs::Domain::DateTime::fromISO("2024-02-20T00:00:00");

    ASSERT_TRUE(dateTime1 > dateTime2);
}

TEST(DateTimeTest, operator_greater_than_returns_false_if_value_is_not_greater_than_other)
{
    TicketOs::Domain::DateTime dateTime1 = TicketOs::Domain::DateTime::fromISO("2024-02-20T00:00:00");
    TicketOs::Domain::DateTime dateTime2 = TicketOs::Domain::DateTime::fromISO("2024-02-20T01:00:00");

    ASSERT_FALSE(dateTime1 > dateTime2);
}

TEST(DateTimeTest, operator_less_than_or_equal_returns_true_if_value_is_less_than_other)
{
    TicketOs::Domain::DateTime dateTime1 = TicketOs::Domain::DateTime::fromISO("2024-02-20T00:00:00");
    TicketOs::Domain::DateTime dateTime2 = TicketOs::Domain::DateTime::fromISO("2024-02-20T01:00:00");

    ASSERT_TRUE(dateTime1 <= dateTime2);
}

TEST(DateTimeTest, operator_less_than_or_equal_returns_true_if_value_is_equal_to_other)
{
    TicketOs::Domain::DateTime dateTime1 = TicketOs::Domain::DateTime::fromISO("2024-02-20T00:00:00");
    TicketOs::Domain::DateTime dateTime2 = TicketOs::Domain::DateTime::fromISO("2024-02-20T00:00:00");

    ASSERT_TRUE(dateTime1 <= dateTime2);
}

TEST(DateTimeTest, operator_less_than_or_equal_returns_false_if_value_is_not_less_than_or_equal_to_other)
{
    TicketOs::Domain::DateTime dateTime1 = TicketOs::Domain::DateTime::fromISO("2024-02-20T01:00:00");
    TicketOs::Domain::DateTime dateTime2 = TicketOs::Domain::DateTime::fromISO("2024-02-20T00:00:00");

    ASSERT_FALSE(dateTime1 <= dateTime2);
}

TEST(DateTimeTest, operator_greater_than_or_equal_returns_true_if_value_is_greater_than_other)
{
    TicketOs::Domain::DateTime dateTime1 = TicketOs::Domain::DateTime::fromISO("2024-02-20T01:00:00");
    TicketOs::Domain::DateTime dateTime2 = TicketOs::Domain::DateTime::fromISO("2024-02-20T00:00:00");

    ASSERT_TRUE(dateTime1 >= dateTime2);
}

TEST(DateTimeTest, operator_greater_than_or_equal_returns_true_if_value_is_equal_to_other)
{
    TicketOs::Domain::DateTime dateTime1 = TicketOs::Domain::DateTime::fromISO("2024-02-20T00:00:00");
    TicketOs::Domain::DateTime dateTime2 = TicketOs::Domain::DateTime::fromISO("2024-02-20T00:00:00");

    ASSERT_TRUE(dateTime1 >= dateTime2);
}

TEST(DateTimeTest, operator_greater_than_or_equal_returns_false_if_value_is_not_greater_than_or_equal_to_other)
{
    TicketOs::Domain::DateTime dateTime1 = TicketOs::Domain::DateTime::fromISO("2024-02-20T00:00:00");
    TicketOs::Domain::DateTime dateTime2 = TicketOs::Domain::DateTime::fromISO("2024-02-20T01:00:00");

    ASSERT_FALSE(dateTime1 >= dateTime2);
}