#ifndef EVENT_H
#define EVENT_H

#include "Types.h"
#include "Ticket.h"
#include "IntegerId.h"
#include "DateTime.h"

#include <vector>
#include <map>
#include <string>

#ifdef TESTING
#define STANDARD_CAPACITY 218
#define LOW_CAPACITY 100
#define HIGH_CAPACITY 500
#else
#define STANDARD_CAPACITY 2188
#define LOW_CAPACITY 1000
#define HIGH_CAPACITY 5000
#endif

namespace TicketOs {
    namespace Domain {
        class Event {
            private:
                IntegerId id;
                DateTime dateTime;
                std::vector<Ticket> availableTickets;
                IntegerId schedulerId;
                std::string homeTeam;
                std::string awayTeam;
                Event(IntegerId id, DateTime dateTime, std::vector<Ticket> availableTickets, IntegerId schedulerId, std::string homeTeam, std::string awayTeam);
            public:
                static Event create(IntegerId id, DateTime dateTime, std::vector<Ticket> availableTickets, IntegerId schedulerId, std::string homeTeam, std::string awayTeam);
                static Event createStandard(IntegerId id, DateTime dateTime, IntegerId schedulerId, std::string homeTeam, std::string awayTeam);
                static Event createLowCapacity(IntegerId id, DateTime dateTime, IntegerId schedulerId, std::string homeTeam, std::string awayTeam);
                static Event createHighCapacity(IntegerId id, DateTime dateTime, IntegerId schedulerId, std::string homeTeam, std::string awayTeam);
                static Event fromPersistence(
                    std::map<std::string, std::string> data,
                    std::vector<std::map<std::string, std::string>> ticketsData
                );

                IntegerId getId();
                DateTime getDateTime();
                std::vector<Ticket> getAvailableTickets();
                IntegerId getSchedulerId();
                std::string getHomeTeam();
                std::string getAwayTeam();

                std::map<std::string, std::string> toData();
        };
    }
}

#endif