#ifndef TICKET_H
#define TICKET_H

#include "Types.h"
#include "IntegerId.h"

#include <map>
#include <string>

namespace TicketOs {
    namespace Domain {
        class Ticket {
            private:
                IntegerId id;
                Types::sectorType sector;
                unsigned short int row;
                unsigned short int seat;
                IntegerId eventId;
                IntegerId *ownerId;

                Ticket(IntegerId id, Types::sectorType sector, unsigned short int row, unsigned short int seat, IntegerId eventId, IntegerId *ownerId);
            public:
                static Ticket create(IntegerId id, Types::sectorType sector, unsigned short int row, unsigned short int seat, IntegerId eventId, IntegerId *owner);
                static Ticket fromPersistence(std::map<std::string, std::string> data);

                IntegerId getId();
                Types::sectorType getSector();
                unsigned short int getRow();
                unsigned short int getSeat();
                IntegerId getEventId();
                IntegerId *getOwnerId();

                bool isAvailable();
                Types::errorType reserve(IntegerId *ownerId);
                Types::errorType release();

                std::map<std::string, std::string> toData();
        };
    }
}

#endif