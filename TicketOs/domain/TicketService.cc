#include "TicketService.h"

#include "Ticket.h"
#include "../infrastructure/binary/BinaryIdRepository.h"

#include <vector>
#include <map>
#include <string>
#include <stdexcept>

namespace TicketOs {
    namespace Service {
        std::vector<Domain::Ticket> TicketService::buildAmountOfTickets(Domain::IntegerId eventId, unsigned int amount, std::string path) {
            Infrastructure::BinaryIdRepository idRepository(path);

            std::vector<Domain::Ticket> tickets;

            std::map<Types::sectorType, unsigned int> ticketTypesAmount = {};
            ticketTypesAmount[Types::sectorType::honorTribune] = amount * HONOR_TRIBUNE_RATIO;
            ticketTypesAmount[Types::sectorType::vipArea] = amount * VIP_AREA_RATIO;
            ticketTypesAmount[Types::sectorType::associatesArea] = amount * ASSOCIATES_AREA_RATIO;
            ticketTypesAmount[Types::sectorType::boxes] = amount * BOXES_RATIO;
            ticketTypesAmount[Types::sectorType::nonAssociatesArea] = amount * NON_ASSOCIATES_AREA_RATIO;
            ticketTypesAmount[Types::sectorType::visitorArea] = amount * VISITOR_AREA_RATIO;
            ticketTypesAmount[Types::sectorType::pressArea] = amount * PRESS_AREA_RATIO;
            
            unsigned int totalTickets = 0;
            for (auto const& ticketTypeAmount : ticketTypesAmount) {
                auto ticketType = ticketTypeAmount.first;
                auto ticketAmount = ticketTypeAmount.second;
                for (unsigned int i = 0; i < ticketAmount; i++) {
                    tickets.push_back(
                        Domain::Ticket::create(
                            idRepository.nextId(),
                            ticketType,
                            i % NUMBER_OF_ROWS,
                            i / NUMBER_OF_ROWS,
                            eventId,
                            nullptr
                        )
                    );
                    totalTickets++;
                }
            }
            
            if (totalTickets > amount) {
                throw std::runtime_error("Amount of tickets created is different from the requested amount, this could be due to sector ratios not adding up to 1");
            }

            /**
             * If due to rounding errors the total amount of tickets is less than the requested amount, we add the missing tickets to the nonAssociatesArea
             */
            if (totalTickets < amount) {
                for (unsigned int i = totalTickets; i < amount; i++) {
                    tickets.push_back(
                        Domain::Ticket::create(
                            idRepository.nextId(),
                            Types::sectorType::nonAssociatesArea,
                            i % NUMBER_OF_ROWS,
                            i / NUMBER_OF_ROWS,
                            eventId,
                            nullptr
                        )
                    );
                }
            }
            return tickets;
        }
    }
}