#ifndef TICKET_SERVICE_H
#define TICKET_SERVICE_H

#include "Ticket.h"
#include "Types.h"

#include "../infrastructure/binary/BinaryIdRepository.h"

#include <vector>

#define HONOR_TRIBUNE_RATIO 0.05
#define VIP_AREA_RATIO 0.05
#define ASSOCIATES_AREA_RATIO 0.2
#define BOXES_RATIO 0.1
#define NON_ASSOCIATES_AREA_RATIO 0.5
#define VISITOR_AREA_RATIO 0.08
#define PRESS_AREA_RATIO 0.02

#define NUMBER_OF_ROWS 10

namespace TicketOs {
    namespace Service {
        class TicketService {
            public:
                static std::vector<Domain::Ticket> buildAmountOfTickets(Domain::IntegerId eventId, unsigned int amount, std::string path = DEFAULT_BINARY_ID_REPOSITORY_PATH);
        };
    }
}

#endif