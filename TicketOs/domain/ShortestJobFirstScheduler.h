#ifndef SHORTEST_JOB_FIRST_SCHEDULER_H
#define SHORTEST_JOB_FIRST_SCHEDULER_H

#include "Scheduler.h"
#include "Client.h"
#include "ClientHistory.h"
#include "Event.h"

#include "../infrastructure/binary/BinaryIdRepository.h"
#include "../infrastructure/binary/BinaryClientRepository.h"

#include "../utility/miliTimer.h"

#include <vector>
#include <thread>
#include <mutex>
#include <chrono>
#include <condition_variable>
#include <iostream>
#include <algorithm>

namespace TicketOs {
    namespace Domain {
        class ShortestJobFirstScheduler : public Scheduler {
            private:
                std::vector<Client> clients;
                int maximumConcurrentClients = 1;
                std::mutex &printLock;
            public:
                ShortestJobFirstScheduler(
                    std::vector<Client> clients,
                    std::mutex &printLock,
                    int maximumConcurrentClients = 1
                );

                ~ShortestJobFirstScheduler() override = default;

                void run(
                    Event event
                ) override;

                static void processClient(
                    Client *client,
                    ClientHistory *clientHistory,
                    int *numberOfConcurrentClients,
                    int *numberOfFinishedClients,
                    int *numberOfStarvingClients,
                    int maximumConcurrentClients,
                    int maximumNumberOfTickets,
                    std::mutex &concurrentLock,
                    std::condition_variable_any &concurrentCondition,
                    std::mutex &printLock
                );
        };
    }
}

#endif