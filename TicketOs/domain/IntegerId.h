#ifndef INTEGER_ID_H
#define INTEGER_ID_H

namespace TicketOs {
    namespace Domain {
        class IntegerId {
            private:
                unsigned int value;
                IntegerId(unsigned int value);
            public:
                static IntegerId fromInteger(unsigned int value);
                unsigned int toInteger();
                bool operator==(const IntegerId& other);
                bool operator!=(const IntegerId& other);
                bool operator<(const IntegerId& other);
                bool operator>(const IntegerId& other);
                bool operator<=(const IntegerId& other);
                bool operator>=(const IntegerId& other);
                IntegerId operator+(const IntegerId& other);
                IntegerId operator-(const IntegerId& other);
                IntegerId operator*(const IntegerId& other);
                IntegerId operator/(const IntegerId& other);
                IntegerId operator%(const IntegerId& other);
                IntegerId operator+=(const IntegerId& other);
                IntegerId operator-=(const IntegerId& other);
                IntegerId operator*=(const IntegerId& other);
                IntegerId operator/=(const IntegerId& other);
                IntegerId operator%=(const IntegerId& other);
                IntegerId operator++();
                IntegerId operator++(int);
                IntegerId operator--();
                IntegerId operator--(int);
        };
    }
}

#endif
