#include "ShortestCompletionScheduler.h"

namespace TicketOs {
    namespace Domain {
        ShortestCompletionScheduler::ShortestCompletionScheduler(
            std::vector<Client> originalClients,
            int maximumConcurrentClients,
            std::mutex &printLock
        ) : originalClients(originalClients), maximumConcurrentClients(maximumConcurrentClients), printLock(printLock) {}

        void ShortestCompletionScheduler::process(
            IntegerId id,
            std::list<Client *> &clients,
            std::list<IntegerId> &finishedClients,
            unsigned int &numberOfStarvingClients,
            unsigned int &numberOfFinishedClients,
            unsigned int maximumNumberOfClients,
            unsigned int maximumNumberOfTickets,
            std::mutex &queueLock,
            std::condition_variable_any &queueChangeCondition,
            std::condition_variable_any &waitingClientProcessCondition,
            std::mutex &printLock
        ) {
            std::cout << Utility::miliTimer() << "[PROCESS] [INFO] Thread " << id.toInteger() << " is running" << std::endl;
            Client *runningClient = nullptr; /** Initialize the running client to null */
            std::cout << Utility::miliTimer() << "[PROCESS] Initialized runningClient" << std::endl;
            ClientHistory *runningClientHistory = nullptr; /** Initialize the running client history to null */
            std::cout << Utility::miliTimer() << "[PROCESS] Initialized runningClientHistory" << std::endl;
            while (numberOfFinishedClients < maximumNumberOfClients) { /** While there are still clients to process */
                std::cout << Utility::miliTimer() << "[PROCESS] Processing clients" << std::endl;
                queueLock.lock(); /** Lock the queue */
                std::cout << Utility::miliTimer() << "[PROCESS] Locked queue" << std::endl;
                waitingClientProcessCondition.notify_all(); /** Notify all threads that the processor is processing a client */
                queueChangeCondition.wait(queueLock); /** Wait for a change in the queue */
                std::cout << Utility::miliTimer() << "[PROCESS] Queue changed" << std::endl;
                if (runningClient != nullptr) {
                    std::cout << Utility::miliTimer() << "[PROCESS] Running client is not null" << std::endl;
                    runningClient->pause(); /** Pause the running client */
                    std::cout << Utility::miliTimer() << "[PROCESS] Paused running client" << std::endl;
                    std::cout <<
                        "[PROCESS] \t\t[ACTION] Process ("
                        << id.toInteger()
                        << "): Client "
                        << runningClient->getId().toInteger()
                        << " has yet: " << runningClient->getTimeToBuyTicket().count()
                        << "ms to complete"
                        << std::endl;

                    if (runningClient->getTimeToBuyTicket() <= std::chrono::milliseconds(0)) { /** The client has finished */
                        std::cout << Utility::miliTimer() << "[PROCESS] Client has finished" << std::endl;
                        /** Do not return it to the queue and increment the number of finished clients */
                        numberOfFinishedClients++;
                        runningClientHistory->setConclusionTime();
                        std::cout << Utility::miliTimer() << "[PROCESS] Set conclusion time" << std::endl;
                        runningClientHistory->setSuccessful();
                        
                        std::cout << Utility::miliTimer() << "[PROCESS] Set successful" << std::endl;
                        finishedClients.push_back(runningClient->getId());
                        printLock.lock();
                        std::cout
                            << Utility::miliTimer()
                            << "[PROCESS] \t\t[ACTION] Process ("
                            << id.toInteger()
                            << "): Client "
                            << runningClient->getId().toInteger()
                            << " has finished buying a ticket"
                            << std::endl;
                        printLock.unlock();
                    } else {
                        //clients.push_back(runningClient); /** Add the running client back to the queue */
                        std::cout << Utility::miliTimer() << "[PROCESS] Added running client back to the queue" << std::endl;
                        
                        printLock.lock();
                        std::cout
                            << Utility::miliTimer()
                            << "[PROCESS] \t\t[ACTION] Process ("
                            << id.toInteger()
                            << "): Client "
                            << runningClient->getId().toInteger()
                            << " has been paused"
                            << std::endl;

                        printLock.unlock();
                        std::cout << Utility::miliTimer() << "[PROCESS] Printed action" << std::endl;
                    }
                }
                runningClient = nullptr; /** Reset the running client */
                std::cout << Utility::miliTimer() << "[PROCESS] Reset running client" << std::endl;
                runningClientHistory = nullptr; /** Reset the running client history */
                std::cout << Utility::miliTimer() << "[PROCESS] Reset running client history" << std::endl;

                printLock.lock();
                std::cout
                    << Utility::miliTimer()
                    << "[PROCESS] \t\t[ACTION] Process ("
                    << id.toInteger()
                    << "): Searching for the shortest completion time"
                    << std::endl;
                printLock.unlock();

                /** Find the client with the shortest completion time */
                for (auto it = clients.begin(); it != clients.end(); it++) {
                    std::cout << Utility::miliTimer() << "[PROCESS] Checking client " << (*it)->getId().toInteger() << std::endl;

                    if ((*it)->getTimeToBuyTicket() <= std::chrono::milliseconds(0)) {
                        std::cout << "[PROCESS] Client already finished" << std::endl;
                        continue;
                    }

                    if (runningClient == nullptr) {
                        std::cout << Utility::miliTimer() << "[PROCESS] Running client is null" << std::endl;
                        runningClient = *it;
                        std::cout << Utility::miliTimer() << "[PROCESS] Set running client" << std::endl;
                        std::cout << Utility::miliTimer() << "TTC: " << (*it)->getTimeToBuyTicket().count() << std::endl;
                    } else {
                        std::cout << Utility::miliTimer() << "[PROCESS] Running client is not null" << std::endl;
                        std::chrono::milliseconds runningClientTimeToCompletion = runningClient->getTimeToBuyTicket();
                        std::cout << Utility::miliTimer() << "[PROCESS] Got running client time to completion" << std::endl;
                        std::chrono::milliseconds itTimeToCompletion = (*it)->getTimeToBuyTicket();
                        std::cout << Utility::miliTimer() << "[PROCESS] Got it time to completion" << std::endl;
                        std::cout << Utility::miliTimer() << "TTC: " << itTimeToCompletion.count() << std::endl;
                        if (itTimeToCompletion < runningClientTimeToCompletion) {
                            std::cout << Utility::miliTimer() << "[PROCESS] It has a shorter completion time" << std::endl;
                            runningClient = *it;
                            std::cout << Utility::miliTimer() << "[PROCESS] Set running client" << std::endl;
                        }
                    }
                }
                
                std::cout << Utility::miliTimer() << "[PROCESS] Finished checking clients" << std::endl;

                if (runningClient == nullptr) {
                    std::cout << Utility::miliTimer() << "[PROCESS] Running client is null" << std::endl;
                    printLock.lock();
                    std::cout
                        << Utility::miliTimer()
                        << "[PROCESS] \t\t[ACTION] Process ("
                        << id.toInteger()
                        << "): No clients to process"
                        << std::endl;
                    printLock.unlock();
                    queueLock.unlock(); /** Unlock the queue */
                    continue;
                }

                /** Here the client is guaranteed to be found */
                std::cout << Utility::miliTimer() << "[PROCESS] Client found" << std::endl;
                //auto runningClientIndex = std::find(clients.begin(), clients.end(), runningClient);
                //std::cout << Utility::miliTimer() << "[PROCESS] Found client index" << std::endl;
                //clients.erase(runningClientIndex); /** Remove the running client from the queue */
                //std::cout << Utility::miliTimer() << "[PROCESS] Erased client" << std::endl;

                /** Gets, from the client, the history that does not have a ConclusionTime */
                std::cout << Utility::miliTimer() << "[PROCESS] runningClient " << runningClient->getId().toInteger() << std::endl;
                std::vector<ClientHistory> clientHistories = runningClient->getClientHistories();
                std::cout << Utility::miliTimer() << "[PROCESS] Got client histories" << std::endl;
                for (auto it = clientHistories.begin(); it != clientHistories.end(); it++) {
                    std::cout << Utility::miliTimer() << "[PROCESS] Checking client history" << std::endl;
                    if (it->getConclusionTime() == nullptr) {
                        std::cout << Utility::miliTimer() << "[PROCESS] Client history has no conclusion time" << std::endl;
                        runningClientHistory = &(*it);
                        std::cout << Utility::miliTimer() << "[PROCESS] Set running client history" << std::endl;
                        break;
                    }
                }

                if (runningClientHistory == nullptr) {
                    std::cout << Utility::miliTimer() << "[PROCESS] Running client does not have a history" << std::endl;
                    queueLock.unlock(); /** Unlock the queue */
                    continue;
                }

                /** If the client has not been responded to yet, set the response time */
                if (!runningClientHistory->isResponded()) {
                    std::cout << Utility::miliTimer() << "[PROCESS] Client has not been responded to" << std::endl;
                    printLock.lock();
                    std::cout
                        << Utility::miliTimer()
                        << "[PROCESS] \t\t[ACTION] Process ("
                        << id.toInteger()
                        << "): Client "
                        << runningClient->getId().toInteger()
                        << " first response from the scheduler"
                        << std::endl;
                    printLock.unlock();

                    runningClientHistory->setResponseTime();
                    std::cout << Utility::miliTimer() << "[PROCESS] Set response time" << std::endl;
                }

                queueLock.unlock(); /** Unlock the queue */
                std::cout << Utility::miliTimer() << "[PROCESS] Unlocked queue" << std::endl;

                printLock.lock();
                std::cout << Utility::miliTimer()
                    << "[PROCESS] \t\t[ACTION] Process: Client "
                    << runningClient->getId().toInteger()
                    << " with "
                    << runningClient->getTimeToBuyTicket().count()
                    << "ms to complete is now running"
                    << std::endl;

                printLock.unlock();
                runningClient->resume(); /** Resume the running client */
                std::cout << Utility::miliTimer() << "[PROCESS] Resumed running client" << std::endl;
                std::cout << Utility::miliTimer() << "[PROCESS] Number of finished clients: " << numberOfFinishedClients << std::endl;
            }

            std::cout << "Finished process" << std::endl;
        }

        void ShortestCompletionScheduler::watcher(
            std::vector<Client> &originalClients,
            std::list<IntegerId> &finishedClients,
            unsigned int &numberOfFinishedClients,
            unsigned int maximumNumberOfClients,
            std::mutex &queueLock,
            std::condition_variable_any &queueChangeCondition,
            std::condition_variable_any &waitingClientProcessCondition,
            std::mutex &printLock
        ) {
            // std::cout << Utility::miliTimer() << "[INFO] Watcher is running" << std::endl;
            // std::cout << Utility::miliTimer() << "Initialized finished clients" << std::endl;
            while (numberOfFinishedClients < maximumNumberOfClients) { /** While there are still clients to process */
                //std::cout << numberOfFinishedClients << " < " << maximumNumberOfClients << std::endl;
                // std::cout << Utility::miliTimer() << "Watching clients" << std::endl;
                for (auto it = originalClients.begin(); it != originalClients.end(); it++) { /** For each client */
                    //std::cout << Utility::miliTimer() << "[Watcher] Checking client " << it->getId().toInteger() << std::endl;

                    if (it->getStartTime() == nullptr) { /** If the client has not started yet */
                        //std::cout << Utility::miliTimer() << "[Watcher] Client has not started" << std::endl;
                        continue;
                    }

                    std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
                    //std::cout << Utility::miliTimer() << "[Watcher] Got current time" << std::endl;
                    std::chrono::milliseconds liveTimeToCompletion = it->getTimeToBuyTicket() - std::chrono::duration_cast<std::chrono::milliseconds>(now - *(it->getStartTime()));
                    //std::cout << Utility::miliTimer() << "[Watcher] Got live time to completion" << std::endl;

                    //std::cout << Utility::miliTimer() << "[Watcher] Client " << it->getId().toInteger() << " has " << liveTimeToCompletion.count() << "ms to complete" << std::endl;

                    if (std::find(finishedClients.begin(), finishedClients.end(), it->getId()) != finishedClients.end()) { /** If the client has already finished */
                        //std::cout << Utility::miliTimer() << "[Watcher] Client has already finished" << std::endl;
                        continue;
                    }

                    if (liveTimeToCompletion.count() <= 0) { /** If the client has finished buying a ticket */
                        //std::cout << Utility::miliTimer() << "[Watcher] Client has finished" << std::endl;
                        queueLock.lock();
                        waitingClientProcessCondition.wait(queueLock); /** Wait for the processor to start processing the client */
                        //std::cout << Utility::miliTimer() << "[Watcher] Notified all threads" << std::endl;
                        queueLock.unlock();
                        queueChangeCondition.notify_all(); /** Notify all threads that the queue has changed */
                        //std::cout << Utility::miliTimer() << "[Watcher] Added client to finished clients" << std::endl;
                    }
                }
            }

            std::cout << "finished watcher" << std::endl;
        }

        void ShortestCompletionScheduler::run(
            Event event
        ) {
            std::cout << Utility::miliTimer() << "[INFO] Running ShortestCompletionScheduler" << std::endl;
            Infrastructure::BinaryIdRepository idRepository;

            std::list<Client *> clients = {};
            std::list<IntegerId> finishedClients = {};
            //std::vector<std::thread> threads = {};

            std::mutex queueLock;
            std::condition_variable_any queueChangeCondition;
            std::condition_variable_any waitingClientProcessCondition;

            std::random_device randomDevice;
            std::mt19937 randomGenerator(randomDevice());
            std::uniform_int_distribution<int> randomDistribution(0, 100);

            unsigned int numberOfStarvingClients = 0;

            /**
             * Create the threads to process the clients
             */
            for (unsigned int i = 0; i < this->maximumConcurrentClients; i++) {
                std::cout << Utility::miliTimer() << "[INFO] Creating thread " << i << std::endl;
                std::thread thread(
                    ShortestCompletionScheduler::process,
                    IntegerId::fromInteger(i),
                    std::ref(clients),
                    std::ref(finishedClients),
                    std::ref(numberOfStarvingClients),
                    std::ref(this->numberOfFinishedClients),
                    (unsigned int) this->originalClients.size(),
                    (unsigned int) event.getAvailableTickets().size(),
                    std::ref(queueLock),
                    std::ref(queueChangeCondition),
                    std::ref(waitingClientProcessCondition),
                    std::ref(this->printLock)
                );
                thread.detach();
                //threads.push_back(std::move(thread));
            }
            std::cout << Utility::miliTimer() << "[INFO] Threads created" << std::endl;

            /**
             * Create the thread to watch the clients
             */
            std::cout << Utility::miliTimer() << "[INFO] Creating watcher thread" << std::endl;
            std::thread watcherThread(
                ShortestCompletionScheduler::watcher,
                std::ref(this->originalClients),
                std::ref(finishedClients),
                std::ref(this->numberOfFinishedClients),
                (unsigned int) this->originalClients.size(),
                std::ref(queueLock),
                std::ref(queueChangeCondition),
                std::ref(waitingClientProcessCondition),
                std::ref(this->printLock)
            );
            watcherThread.detach();
            std::cout << Utility::miliTimer() << "[INFO] Watcher thread created" << std::endl;

            /**
             * Spuriously add a random client from the original queue to the clients queue
             */
            std::cout << Utility::miliTimer() << "[INFO] Spuriously adding clients" << std::endl;
            for (unsigned int i = 0; i < this->originalClients.size(); i++) {
                std::cout << Utility::miliTimer() << "Index " << i << std::endl;
                std::cout << Utility::miliTimer() << "Adding client" << std::endl;
                std::cout << Utility::miliTimer() << "Locked queue" << std::endl;
                Client *client = &(this->originalClients[i]);
                std::cout << Utility::miliTimer() << "Got client" << std::endl;

                /** Create a client history for the client as soon as it joins the queue */
                ClientHistory clientHistory = ClientHistory::create(
                    idRepository.nextId(),
                    client->getId(),
                    event.getId()
                );
                std::cout << Utility::miliTimer() << "Created client history" << std::endl;
                client->addHistory(clientHistory);
                std::cout << Utility::miliTimer() << "Added history to client" << std::endl;

                queueLock.lock(); /** Lock the queue */
                clients.push_back(client); /** Add the client to the run queue */
                std::cout << Utility::miliTimer() << "Added client to run queue" << std::endl;
                queueLock.unlock(); /** Unlock the queue */
                std::cout << Utility::miliTimer() << "Unlocked queue" << std::endl;
                queueChangeCondition.notify_all(); /** Notify all threads that the queue has changed */
                std::cout << Utility::miliTimer() << "Notified all threads" << std::endl;

                printLock.lock();
                std::cout << Utility::miliTimer() << "[INFO] Client " << client->getId().toInteger() << " has joined the fray" << std::endl;
                printLock.unlock();

                /**
                 * Sleep for a random amount of time
                 */
                std::cout << Utility::miliTimer() << "Sleeping" << std::endl;
                std::this_thread::sleep_for(std::chrono::milliseconds(
                    randomDistribution(randomGenerator) + 5
                ));
                std::cout << Utility::miliTimer() << "Slept" << std::endl;
            }

            //for (std::thread &thread : threads) {
            //    std::cout << Utility::miliTimer() << "[INFO] Joining thread" << std::endl;
            //    thread.join();
            //}

            while (numberOfFinishedClients < this->originalClients.size()) {
                std::this_thread::sleep_for(std::chrono::milliseconds(10));
            }

            std::cout << Utility::miliTimer() << "[INFO] Finished scheduler" << std::endl;
        }
    }
}