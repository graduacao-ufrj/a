#include "DateTime.h"

#include <chrono>
#include <string>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <sstream>

namespace TicketOs {
    namespace Domain {
        DateTime::DateTime(std::chrono::system_clock::time_point value) {
            this->value = value;
        }

        DateTime DateTime::fromISO(std::string iso) {
            if (iso.length() == 10) {
                iso += "T00:00:00";
            }

            std::tm time = {};
            std::istringstream ss(iso);
            ss >> std::get_time(&time, ISO_FORMAT);

            if (ss.fail()) {
                throw std::invalid_argument("Invalid ISO format");
            }

            auto tp = std::chrono::system_clock::from_time_t(std::mktime(&time));
            return DateTime(tp);
        }

        DateTime DateTime::fromTimeT(std::time_t value) {
            auto tp = std::chrono::system_clock::from_time_t(value);
            return DateTime(tp);
        }

        DateTime DateTime::fromTm(std::tm* time) {
            auto tp = std::chrono::system_clock::from_time_t(std::mktime(time));
            return DateTime(tp);
        }

        DateTime DateTime::now() {
            return DateTime(std::chrono::system_clock::now());
        }

        std::string DateTime::toISO() {
            auto time = std::chrono::system_clock::to_time_t(this->value);
            std::ostringstream oss;
            oss << std::put_time(std::localtime(&time), ISO_FORMAT);
            return oss.str();
        }

        bool DateTime::operator==(const DateTime& other) {
            return value == other.value;
        }

        bool DateTime::operator!=(const DateTime& other) {
            return value != other.value;
        }

        bool DateTime::operator<(const DateTime& other) {
            return value < other.value;
        }

        bool DateTime::operator>(const DateTime& other) {
            return value > other.value;
        }

        bool DateTime::operator<=(const DateTime& other) {
            return value <= other.value;
        }

        bool DateTime::operator>=(const DateTime& other) {
            return value >= other.value;
        }

        std::chrono::milliseconds DateTime::operator-(const DateTime& other) {
            auto duration = this->value - other.value;
            return std::chrono::duration_cast<std::chrono::milliseconds>(duration);
        }
    }
}