#include "FirstComeFirstServedScheduler.h"

#include "Client.h"
#include "ClientHistory.h"
#include "Event.h"

#include "../infrastructure/binary/BinaryIdRepository.h"
#include "../infrastructure/binary/BinaryClientRepository.h"

#include "../utility/miliTimer.h"

#include <iostream>
#include <vector>
#include <thread>
#include <mutex>
#include <chrono>
#include <condition_variable>
#include <random>

namespace TicketOs {
    namespace Domain {
        FirstComeFirstServedScheduler::FirstComeFirstServedScheduler(
            std::vector<TicketOs::Domain::Client> clients,
            std::mutex &printLock,
            int maximumConcurrentClients
        ) : clients(clients),
        maximumConcurrentClients(maximumConcurrentClients),
        printLock(printLock) {}

        void FirstComeFirstServedScheduler::run(Event event) {
            this->printLock.lock();
            std::cout << Utility::miliTimer() << "\t[INFO] Running First Come First Served Scheduler" << std::endl;
            this->printLock.unlock();

            Infrastructure::BinaryIdRepository idRepository;
            Infrastructure::BinaryClientRepository clientRepository;

            std::vector<ClientHistory *> clientHistories;

            std::mutex concurrentLock;
            std::condition_variable_any concurrentCondition;
            std::vector<std::thread> clientThreads;

            int numberOfConcurrentClients = 0;
            int numberOfSuccessfulClients = 0;
            int numberOfStarvingClients = 0;
            const std::size_t totalNumberOfClients = clients.size();

            this->printLock.lock();
            std::cout << Utility::miliTimer() << "\t[INFO] Total number of tickets: " << event.getAvailableTickets().size() << std::endl;
            this->printLock.unlock();

            std::chrono::time_point<std::chrono::system_clock> startTime = std::chrono::system_clock::now();
            for (std::size_t i = 0; i < totalNumberOfClients; i++) {
                TicketOs::Domain::Client *client = nullptr;
                client = &clients[i];

                ClientHistory *clientHistory = new ClientHistory(ClientHistory::create(
                    idRepository.nextId(),
                    client->getId(),
                    event.getId()
                ));

                clientHistories.push_back(clientHistory);

                while (numberOfConcurrentClients >= maximumConcurrentClients) {
                    /** Stop creating new threads if the maximum number of concurrent clients has been reached */
                    concurrentLock.lock();
                    numberOfStarvingClients += client->waitInLine(
                        std::ref(concurrentLock),
                        std::ref(concurrentCondition),
                        std::ref(this->printLock)
                    );
                    concurrentLock.unlock();
                }

                /** Allow client to enter the system to buy the ticket */
                std::thread processClientThread(
                    FirstComeFirstServedScheduler::processClient,
                    client,
                    clientHistory,
                    &numberOfConcurrentClients,
                    &numberOfSuccessfulClients,
                    &numberOfStarvingClients,
                    maximumConcurrentClients,
                    event.getAvailableTickets().size(),
                    std::ref(concurrentLock), // Use std::ref for reference
                    std::ref(concurrentCondition), // Use std::ref for reference
                    std::ref(this->printLock) // Assuming this also needs to be passed by reference
                );
                clientThreads.push_back(std::move(processClientThread));
            }

            for (std::size_t i = 0; i < totalNumberOfClients; i++) {
                clientThreads[i].join();
                clients[i].addHistory(*clientHistories[i]);
            }

            std::chrono::time_point<std::chrono::system_clock> endTime = std::chrono::system_clock::now();

            // All clients have been processed
            std::chrono::duration<double> elapsedSeconds = endTime - startTime;
            this->printLock.lock();
            std::cout << Utility::miliTimer() << "\t[INFO] All clients have been processed in " << std::to_string(elapsedSeconds.count() * 1000) << "ms" << std::endl;
            this->printLock.unlock();

            std::chrono::milliseconds meanResponseTime = std::chrono::milliseconds(0);
            std::chrono::milliseconds meanConclusionTime = std::chrono::milliseconds(0);
            for (ClientHistory *clientHistory : clientHistories) {
                meanResponseTime += (*(clientHistory->getResponseTime()) - clientHistory->getArrivalTime());
                meanConclusionTime += (*(clientHistory->getConclusionTime()) - clientHistory->getArrivalTime());
            }

            meanResponseTime /= clientHistories.size();
            meanConclusionTime /= clientHistories.size();

            this->printLock.lock();
            std::cout << Utility::miliTimer() << "\t[INFO] Mean response time: " << std::to_string(meanResponseTime.count()) << "ms" << std::endl;
            std::cout << Utility::miliTimer() << "\t[INFO] Mean conclusion time: " << std::to_string(meanConclusionTime.count()) << "ms" << std::endl;
            std::cout << Utility::miliTimer() << "\t[INFO] Number of starved clients: " << std::to_string(numberOfStarvingClients) << std::endl;
            this->printLock.unlock();

            for (Client client : clients) {
                clientRepository.save(client);
            }
        }

        void FirstComeFirstServedScheduler::processClient(
            Client *client,
            ClientHistory *clientHistory,
            int *numberOfConcurrentClients,
            int *numberOfSuccessfulClients,
            int *numberOfStarvingClients,
            int maximumConcurrentClients,
            int maximumNumberOfTickets,
            std::mutex &concurrentLock,
            std::condition_variable_any &concurrentCondition,
            std::mutex &printLock
        ) {
            /** Gets the lock to allow the client to enter the system */
            concurrentLock.lock();
            printLock.lock();
            std::cout << Utility::miliTimer() << "\t\t[INFO] Number of finished clients: " << std::to_string(*numberOfSuccessfulClients) << std::endl;
            std::cout << Utility::miliTimer() << "\t\t[INFO] Maximum number of tickets: " << std::to_string(maximumNumberOfTickets) << std::endl;
            printLock.unlock();
            if (*numberOfSuccessfulClients >= maximumNumberOfTickets) {
                if (clientHistory->getResponseTime() == nullptr) {
                    clientHistory->setResponseTime();
                }
                clientHistory->setFailed();
                printLock.lock();
                std::cout << Utility::miliTimer() << "\t\t[WARNING] All tickets have been sold!" << std::endl;
                printLock.unlock();
                concurrentCondition.notify_one();
                concurrentLock.unlock();
                return;
            }

            /** Now with the lock, the client can enter the system */
            /** Here this is done by incrementing the number of concurrent clients */
            *numberOfConcurrentClients += 1;
            clientHistory->setResponseTime();
            printLock.lock();
            std::cout << Utility::miliTimer() << "\t\t[INFO] Number of concurrent clients: " << std::to_string(*numberOfConcurrentClients) << std::endl;
            printLock.unlock();
            concurrentLock.unlock(); /** Release the lock */

            /** Process the client */
            /** Here this is done by sleeping for the client to finish */
            std::chrono::milliseconds usageTime = client->getUsageTime();
            printLock.lock();
            std::cout << Utility::miliTimer() << "\t\t[ACTION] Client " << client->getId().toInteger() << " with usage time " << usageTime.count() << "ms started" << std::endl;
            printLock.unlock();
            while (client->buyingTicket()) {
                // Do additional checks here
            }
            printLock.lock();
            std::cout << Utility::miliTimer() << "\t\t[ACTION] Client " << client->getId().toInteger() << " with usage time " << usageTime.count() << "ms finished" << std::endl;
            printLock.unlock();

            /** The client has finished, so it can leave the system */
            concurrentLock.lock(); /** Get the lock to allow the client to leave the system */
            /** Here this is done by decrementing the number of concurrent clients */
            clientHistory->setConclusionTime();

            if (*numberOfSuccessfulClients >= maximumNumberOfTickets) {
                clientHistory->setFailed();
                printLock.lock();
                std::cout << Utility::miliTimer() << "\t\t[WARNING] Client " << client->getId().toInteger() << " failed to buy a ticket" << std::endl;
                printLock.unlock();
                numberOfConcurrentClients -= 1;
                concurrentCondition.notify_one();
                concurrentLock.unlock();
                return;
            }
            
            clientHistory->setSuccessful();
            *numberOfConcurrentClients -= 1;
            *numberOfSuccessfulClients += 1; /** Increment the number of finished clients */
            concurrentCondition.notify_one(); /** Notify that a client has finished */
            concurrentLock.unlock(); /** Release the lock to allow the next client to enter the system */
        }
            
    }
}
