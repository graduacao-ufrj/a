#include "Event.h"

#include "Types.h"
#include "Ticket.h"
#include "IntegerId.h"
#include "DateTime.h"
#include "TicketService.h"

#include <vector>
#include <map>
#include <string>
#include <stdexcept>

namespace TicketOs {
    namespace Domain {
        Event::Event(
            IntegerId id,
            DateTime dateTime,
            std::vector<Ticket> availableTickets,
            IntegerId schedulerId,
            std::string homeTeam,
            std::string awayTeam
        ): id(id), dateTime(dateTime), availableTickets(availableTickets), schedulerId(schedulerId), homeTeam(homeTeam), awayTeam(awayTeam) {};

        Event Event::create(
            IntegerId id,
            DateTime dateTime,
            std::vector<Ticket> availableTickets,
            IntegerId schedulerId,
            std::string homeTeam,
            std::string awayTeam
        ) {
            return Event(id, dateTime, availableTickets, schedulerId, homeTeam, awayTeam);
        }

        Event Event::createStandard(
            IntegerId id,
            DateTime dateTime,
            IntegerId schedulerId,
            std::string homeTeam,
            std::string awayTeam
        ) {
            std::vector<Ticket> tickets = Service::TicketService::buildAmountOfTickets(id, STANDARD_CAPACITY); 
            return Event(id, dateTime, tickets, schedulerId, homeTeam, awayTeam);
        }

        Event Event::createLowCapacity(
            IntegerId id,
            DateTime dateTime,
            IntegerId schedulerId,
            std::string homeTeam,
            std::string awayTeam
        ) {
            std::vector<Ticket> tickets = Service::TicketService::buildAmountOfTickets(id, LOW_CAPACITY);
            return Event(id, dateTime, tickets, schedulerId, homeTeam, awayTeam);
        }

        Event Event::createHighCapacity(
            IntegerId id,
            DateTime dateTime,
            IntegerId schedulerId,
            std::string homeTeam,
            std::string awayTeam
        ) {
            std::vector<Ticket> tickets = Service::TicketService::buildAmountOfTickets(id, HIGH_CAPACITY, "id_sequence");
            return Event(id, dateTime, tickets, schedulerId, homeTeam, awayTeam);
        }

        Event Event::fromPersistence(
            std::map<std::string, std::string> data,
            std::vector<std::map<std::string, std::string>> ticketsData
        ) {
            if (data.find("id") == data.end()) {
                throw std::invalid_argument("Missing id");
            }

            if (data.find("dateTime") == data.end()) {
                throw std::invalid_argument("Missing dateTime");
            }

            if (data.find("schedulerId") == data.end()) {
                throw std::invalid_argument("Missing schedulerId");
            }

            if (data.find("homeTeam") == data.end()) {
                throw std::invalid_argument("Missing homeTeam");
            }

            if (data.find("awayTeam") == data.end()) {
                throw std::invalid_argument("Missing awayTeam");
            }

            IntegerId id = IntegerId::fromInteger(std::stoi(data["id"]));
            DateTime dateTime = DateTime::fromISO(data["dateTime"]);
            IntegerId schedulerId = IntegerId::fromInteger(std::stoi(data["schedulerId"]));
            std::string homeTeam = data["homeTeam"];
            std::string awayTeam = data["awayTeam"];

            std::vector<Ticket> availableTickets;
            for (std::map<std::string, std::string> ticketData : ticketsData) {
                availableTickets.push_back(Ticket::fromPersistence(ticketData));
            }

            return Event(id, dateTime, availableTickets, schedulerId, homeTeam, awayTeam);
        }

        IntegerId Event::getId() {
            return id;
        }

        DateTime Event::getDateTime() {
            return dateTime;
        }

        std::vector<Ticket> Event::getAvailableTickets() {
            return availableTickets;
        }

        IntegerId Event::getSchedulerId() {
            return schedulerId;
        }

        std::string Event::getHomeTeam() {
            return homeTeam;
        }

        std::string Event::getAwayTeam() {
            return awayTeam;
        }

        std::map<std::string, std::string> Event::toData() {
            std::map<std::string, std::string> data;
            data["id"] = std::to_string(id.toInteger());
            data["dateTime"] = dateTime.toISO();
            data["schedulerId"] = std::to_string(schedulerId.toInteger());
            data["homeTeam"] = homeTeam;
            data["awayTeam"] = awayTeam;

            return data;
        }
    }
}