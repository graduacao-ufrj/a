#ifndef SCHEDULER_H
#define SCHEDULER_H

#include "Event.h"

namespace TicketOs {
    namespace Domain {
        class Scheduler {
            public:
                Scheduler() = default;
                virtual ~Scheduler() = default;
                virtual void run(Event event) = 0;
        };
    }
}

#endif