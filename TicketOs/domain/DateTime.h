#ifndef DATE_TIME_H
#define DATE_TIME_H

#include <chrono>
#include <string>

#define ISO_FORMAT "%Y-%m-%dT%H:%M:%S"

namespace TicketOs {
    namespace Domain {
        class DateTime {
            private:
                std::chrono::system_clock::time_point value;
                DateTime(std::chrono::system_clock::time_point value);
            public:
                static DateTime fromISO(std::string iso);
                static DateTime fromTimeT(std::time_t value);
                static DateTime fromTm(std::tm *time);
                static DateTime now();
                std::string toISO();
                bool operator==(const DateTime& other);
                bool operator!=(const DateTime& other);
                bool operator<(const DateTime& other);
                bool operator>(const DateTime& other);
                bool operator<=(const DateTime& other);
                bool operator>=(const DateTime& other);
                std::chrono::milliseconds operator-(const DateTime& other);
        };
    }
}

#endif