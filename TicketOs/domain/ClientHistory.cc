#include "ClientHistory.h"

#include "Types.h"
#include "IntegerId.h"
#include "DateTime.h"

#include <ctime>
#include <map>
#include <stdexcept>

namespace TicketOs {
    namespace Domain {
        ClientHistory::ClientHistory(
            IntegerId id,
            IntegerId clientId,
            DateTime arrivalTime,
            DateTime *responseTime,
            DateTime *conclusionTime,
            IntegerId eventId,
            bool successful
        ): id(id), clientId(clientId), arrivalTime(arrivalTime), responseTime(responseTime), conclusionTime(conclusionTime), eventId(eventId), successful(successful) {
            if (responseTime != nullptr && *responseTime < arrivalTime) {
                throw std::invalid_argument("Response time cannot be before arrival time");
            }

            if (conclusionTime != nullptr) {
                if (*conclusionTime < arrivalTime) {
                    throw std::invalid_argument("Conclusion time cannot be before arrival time");
                }

                if (responseTime != nullptr && *conclusionTime < *responseTime) {
                    throw std::invalid_argument("Conclusion time cannot be before response time");
                }
            }

            if (successful && conclusionTime == nullptr) {
                throw std::invalid_argument("Successful history must have a conclusion time");
            }
        }

       /*ClientHistory::~ClientHistory() {
            delete responseTime;
            delete conclusionTime;
        }*/

        ClientHistory ClientHistory::create(
            IntegerId id,
            IntegerId clientId,
            IntegerId eventId
        ) {
            return ClientHistory(id, clientId, DateTime::now(), nullptr, nullptr, eventId);
        }

        ClientHistory ClientHistory::fromPersistence(std::map<std::string, std::string> data) {
            if (data.find("id") == data.end()) {
                throw std::invalid_argument("Missing id");
            }

            if (data.find("clientId") == data.end()) {
                throw std::invalid_argument("Missing client id");
            }

            if (data.find("arrivalTime") == data.end()) {
                throw std::invalid_argument("Missing arrival time");
            }

            if (data.find("responseTime") == data.end()) {
                throw std::invalid_argument("Missing response time");
            }

            if (data.find("conclusionTime") == data.end()) {
                throw std::invalid_argument("Missing conclusion time");
            }

            if (data.find("eventId") == data.end()) {
                throw std::invalid_argument("Missing event id");
            }

            if (data.find("successful") == data.end()) {
                throw std::invalid_argument("Missing successful flag");
            }

            return ClientHistory(
                IntegerId::fromInteger(std::stoi(data["id"])),
                IntegerId::fromInteger(std::stoi(data["clientId"])),
                DateTime::fromISO(data["arrivalTime"]),
                new DateTime(DateTime::fromISO(data["responseTime"])),
                new DateTime(DateTime::fromISO(data["conclusionTime"])),
                IntegerId::fromInteger(std::stoi(data["eventId"])),
                data["successful"] == "1"
            );
        }

        bool ClientHistory::wasSuccessful() {
            return this->successful;
        }

        bool ClientHistory::isResponded() {
            return this->responseTime != nullptr;
        }

        bool ClientHistory::isConcluded() {
            return this->conclusionTime != nullptr;
        }

        IntegerId ClientHistory::getId() {
            return this->id;
        }

        IntegerId ClientHistory::getClientId() {
            return this->clientId;
        }

        DateTime ClientHistory::getArrivalTime() {
            return this->arrivalTime;
        }

        DateTime *ClientHistory::getResponseTime() {
            return this->responseTime;
        }

        DateTime *ClientHistory::getConclusionTime() {
            return this->conclusionTime;
        }

        IntegerId ClientHistory::getEventId() {
            return this->eventId;
        }

        Types::errorType ClientHistory::setResponseTime() {
            if (this->responseTime != nullptr) {
                return Types::errorType::clientHistoryAlreadyResponded;
            }

            this->responseTime = new DateTime(DateTime::now());
            return Types::errorType::ok;
        }

        Types::errorType ClientHistory::setConclusionTime() {
            if (this->conclusionTime != nullptr) {
                return Types::errorType::clientHistoryAlreadyConcluded;
            }

            if (this->responseTime == nullptr) {
                return Types::errorType::clientHistoryNotResponded;
            }

            this->conclusionTime = new DateTime(DateTime::now());
            return Types::errorType::ok;
        }

        Types::errorType ClientHistory::setSuccessful() {
            if (conclusionTime == nullptr) {
                this->setConclusionTime();
            }

            this->successful = true;
            return Types::errorType::ok;
        }

        Types::errorType ClientHistory::setFailed() {
            if (conclusionTime == nullptr) {
                this->setConclusionTime();
            }

            this->successful = false;
            return Types::errorType::ok;
        }

        std::map<std::string, std::string> ClientHistory::toData() {
            std::map<std::string, std::string> data;

            data["id"] = std::to_string(id.toInteger());
            data["clientId"] = std::to_string(clientId.toInteger());
            data["arrivalTime"] = arrivalTime.toISO();
            data["responseTime"] = responseTime == nullptr ? "" : responseTime->toISO();
            data["conclusionTime"] = conclusionTime == nullptr ? "" : conclusionTime->toISO();
            data["eventId"] = std::to_string(eventId.toInteger());
            data["successful"] = successful ? "1" : "0";

            return data;
        }
    }
}
