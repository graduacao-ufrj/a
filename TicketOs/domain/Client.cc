#include "Client.h"

#include "Types.h"
#include "IntegerId.h"
#include "ClientHistory.h"

#include "../utility/miliTimer.h"

#include <map>
#include <string>
#include <vector>
#include <stdexcept>
#include <chrono>
#include <iostream>
#include <mutex>
#include <condition_variable>

namespace TicketOs {
    namespace Domain {
        Client::Client(
            TicketOs::Domain::IntegerId id,
            TicketOs::Types::numberOfStars stars,
            std::chrono::milliseconds usageTime,
            std::vector<TicketOs::Domain::ClientHistory> clientHistories
        ): id(id), stars(stars), timeToBuyTicket(usageTime), usageTime(usageTime), clientHistories(clientHistories) {}

        Client Client::create(
            TicketOs::Domain::IntegerId id,
            TicketOs::Types::numberOfStars stars,
            std::chrono::milliseconds usageTime
        ) {
            return Client(
                id,
                stars,
                usageTime,
                std::vector<TicketOs::Domain::ClientHistory>()
            );
        }

        Client Client::fromPersistence(
            std::map<std::string, std::string> data,
            std::vector<std::map<std::string, std::string>> clientHistoriesData
        ) {
            if (data.find("id") == data.end()) {
                throw std::invalid_argument("Missing id");
            }

            if (data.find("stars") == data.end()) {
                throw std::invalid_argument("Missing stars");
            }

            if (data.find("usageTime") == data.end()) {
                throw std::invalid_argument("Missing usageTime");
            }

            TicketOs::Domain::IntegerId id = TicketOs::Domain::IntegerId::fromInteger(std::stoi(data["id"]));
            TicketOs::Types::numberOfStars stars = std::stoi(data["stars"]);
            std::chrono::milliseconds usageTime = std::chrono::milliseconds(std::stoll(data["usageTime"]));

            std::vector<TicketOs::Domain::ClientHistory> clientHistories;
            for (std::map<std::string, std::string> historyData : clientHistoriesData) {
                clientHistories.push_back(TicketOs::Domain::ClientHistory::fromPersistence(historyData));
            }

            return Client(
                id,
                stars,
                usageTime,
                clientHistories
            );
        }

        TicketOs::Domain::IntegerId Client::getId() {
            return id;
        }

        TicketOs::Types::numberOfStars Client::getStars() {
            return stars;
        }

        Types::errorType Client::setStars(TicketOs::Types::numberOfStars stars) {
            this->stars = stars;
            return Types::errorType::ok;
        }

        std::vector<TicketOs::Domain::ClientHistory> Client::getClientHistories() {
            return clientHistories;
        }

        Types::errorType Client::addHistory(TicketOs::Domain::ClientHistory history) {
            if (history.getClientId() != id) {
                return Types::errorType::clientClientHistoryIdDoesNotMatch;
            }

            clientHistories.push_back(history);
            return Types::errorType::ok;
        }

        std::chrono::milliseconds Client::getUsageTime() {
            return usageTime;
        }

        std::chrono::time_point<std::chrono::system_clock> *Client::getStartTime() {
            return startTime;
        }

        /**
         * @brief Simulate a client buying a ticket
         * Applyies a waiting time without blocking the thread
         * Must be called in a loop
         */
        bool Client::buyingTicket() {
            if (this->startTime == nullptr) {
                this->startTime = new std::chrono::time_point<std::chrono::system_clock>(std::chrono::system_clock::now());
            }

            std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
            if (now - *(this->startTime) > std::chrono::milliseconds(this->usageTime)) {
                return false;
            }

            return true;
        }

        /**
         * @brief Wait until the client finishes buying a ticket
         * This will block the thread
         */
        void Client::waitUntilBuyingTicketIsDone() {
            while (this->buyingTicket()) {}
        }

        /**
         * @brief Get how much time is left for the client to finish buying a ticket
         */
        std::chrono::milliseconds Client::getHowMuchTimeIsLeft() {
            if (this->startTime == nullptr) {
                return std::chrono::milliseconds(0);
            }

            std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
            return std::chrono::duration_cast<std::chrono::milliseconds>(this->usageTime - (now - *(this->startTime)));
        }

        bool Client::waitInLine(
            std::mutex &concurrentLock,
            std::condition_variable_any &concurrentCondition,
            std::mutex &printLock
        ) {
            if (this->waitStartTime == nullptr) {
                this->waitStartTime = new std::chrono::time_point<std::chrono::system_clock>(std::chrono::system_clock::now());
                printLock.lock();
                std::cout << Utility::miliTimer() << "\t\t[ACTION] Client " << this->id.toInteger() << " is waiting for a client to finish..." << std::endl;
                printLock.unlock();
            }

            std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();

            if (now - *(this->waitStartTime) > std::chrono::milliseconds(this->usageTime * 5)) {
                this->numberOfWaits += 1;
                *(this->waitStartTime) = now;
                if(this->numberOfWaits == 1) {
                    std::cout << Utility::miliTimer() << "\t\t[WARNING] I am " << this->id.toInteger() << " and I am starving!" << std::endl;
                    return true;
                }
            }
            
            return false;
        }

        void Client::pause() {
            if (this->startTime == nullptr) {
                return;
            }
            std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
            std::chrono::milliseconds timeToCompletion = this->timeToBuyTicket - std::chrono::duration_cast<std::chrono::milliseconds>(now - *(this->startTime));

            this->timeToBuyTicket = timeToCompletion;
        }

        void Client::resume() {
            std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
            
            if (this->startTime == nullptr) {
                this->startTime = new std::chrono::time_point<std::chrono::system_clock>(now);
                return;
            }
            
            *(this->startTime) = now;
        }

        std::chrono::milliseconds Client::getTimeToBuyTicket() {
            return this->timeToBuyTicket;
        }

        bool Client::operator==(const Client& other) {
            return stars == other.stars;
        }

        bool Client::operator>(const Client& other) {
            return stars > other.stars;
        }

        bool Client::operator<(const Client& other) {
            return stars < other.stars;
        }
        
        std::map<std::string, std::string> Client::toData() {
            std::map<std::string, std::string> data;
            data["id"] = std::to_string(id.toInteger());
            data["stars"] = std::to_string(stars);
            return data;
        }
    }
}