#include "IntegerId.h"

namespace TicketOs {
    namespace Domain {
        IntegerId::IntegerId(unsigned int value): value(value) {}

        IntegerId IntegerId::fromInteger(unsigned int value) {
            return IntegerId(value);
        }

        unsigned int IntegerId::toInteger() {
            return this->value;
        }

        bool IntegerId::operator==(const IntegerId& other) {
            return this->value == other.value;
        }

        bool IntegerId::operator!=(const IntegerId& other) {
            return this->value != other.value;
        }

        bool IntegerId::operator<(const IntegerId& other) {
            return this->value < other.value;
        }

        bool IntegerId::operator>(const IntegerId& other) {
            return this->value > other.value;
        }

        bool IntegerId::operator<=(const IntegerId& other) {
            return this->value <= other.value;
        }

        bool IntegerId::operator>=(const IntegerId& other) {
            return this->value >= other.value;
        }

        IntegerId IntegerId::operator+(const IntegerId& other) {
            return IntegerId(this->value + other.value);
        }

        IntegerId IntegerId::operator-(const IntegerId& other) {
            return IntegerId(this->value - other.value);
        }

        IntegerId IntegerId::operator*(const IntegerId& other) {
            return IntegerId(this->value * other.value);
        }

        IntegerId IntegerId::operator/(const IntegerId& other) {
            return IntegerId(this->value / other.value);
        }

        IntegerId IntegerId::operator%(const IntegerId& other) {
            return IntegerId(this->value % other.value);
        }

        IntegerId IntegerId::operator+=(const IntegerId& other) {
            this->value += other.value;
            return *this;
        }

        IntegerId IntegerId::operator-=(const IntegerId& other) {
            this->value -= other.value;
            return *this;
        }

        IntegerId IntegerId::operator*=(const IntegerId& other) {
            this->value *= other.value;
            return *this;
        }

        IntegerId IntegerId::operator/=(const IntegerId& other) {
            this->value /= other.value;
            return *this;
        }

        IntegerId IntegerId::operator%=(const IntegerId& other) {
            this->value %= other.value;
            return *this;
        }

        IntegerId IntegerId::operator++() {
            this->value++;
            return *this;
        }

        IntegerId IntegerId::operator++(int) {
            IntegerId temp = *this;
            this->value++;
            return temp;
        }

        IntegerId IntegerId::operator--() {
            this->value--;
            return *this;
        }

        IntegerId IntegerId::operator--(int) {
            IntegerId temp = *this;
            this->value--;
            return temp;
        }
    }
}