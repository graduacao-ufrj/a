#include "Ticket.h"

#include "Types.h"
#include "IntegerId.h"

#include <map>
#include <iostream>
#include <stdexcept>

namespace TicketOs {
    namespace Domain {
        Ticket::Ticket(
            IntegerId id,
            Types::sectorType sector,
            unsigned short int row,
            unsigned short int seat,
            IntegerId eventId,
            IntegerId *ownerId
        ): id(id), sector(sector), row(row), seat(seat), eventId(eventId), ownerId(ownerId) {}

        Ticket
        Ticket::create(
            IntegerId id,
            Types::sectorType sector,
            unsigned short int row,
            unsigned short int seat,
            IntegerId eventId,
            IntegerId *owner
        ) {
            return Ticket(id, sector, row, seat, eventId, owner);
        }

        Ticket
        Ticket::fromPersistence(
            std::map<std::string, std::string> data
        ) {
            if (data.find("id") == data.end()) {
                throw std::invalid_argument("Missing id");
            }

            if (data.find("sector") == data.end()) {
                throw std::invalid_argument("Missing sector");
            }

            if (data.find("row") == data.end()) {
                throw std::invalid_argument("Missing row");
            }

            if (data.find("seat") == data.end()) {
                throw std::invalid_argument("Missing seat");
            }

            if (data.find("eventId") == data.end()) {
                throw std::invalid_argument("Missing eventId");
            }

            IntegerId id = IntegerId::fromInteger(std::stoi(data["id"]));
            Types::sectorType sector = static_cast<Types::sectorType>(std::stoi(data["sector"]));
            unsigned short int row = std::stoi(data["row"]);
            unsigned short int seat = std::stoi(data["seat"]);
            IntegerId eventId = IntegerId::fromInteger(std::stoi(data["eventId"]));

            if (data.find("ownerId") != data.end() && data["ownerId"] != "") {
                IntegerId *ownerId = new IntegerId(IntegerId::fromInteger(std::stoi(data["ownerId"])));
                return Ticket(id, sector, row, seat, eventId, ownerId);
            }

            return Ticket(id, sector, row, seat, eventId, nullptr);
        }

        IntegerId
        Ticket::getId()
        {
            return this->id;
        }

        Types::sectorType
        Ticket::getSector()
        {
            return this->sector;
        }

        unsigned short int
        Ticket::getRow()
        {
            return this->row;
        }

        unsigned short int
        Ticket::getSeat()
        {
            return this->seat;
        }

        IntegerId
        Ticket::getEventId()
        {
            return this->eventId;
        }

        IntegerId
        *Ticket::getOwnerId()
        {
            return this->ownerId;
        }

        bool
        Ticket::isAvailable()
        {
            return this->ownerId == nullptr;
        }

        Types::errorType
        Ticket::reserve(IntegerId *clientId)
        {
            if (ownerId != nullptr) {
                return Types::errorType::ticketAlreadyReserved;
            }

            if (clientId == nullptr) {
                return Types::errorType::ticketClientNotValid;
            }

            this->ownerId = clientId;
            return Types::errorType::ok;
        }

        Types::errorType
        Ticket::release()
        {
            if (ownerId == nullptr) {
                return Types::errorType::ticketNotReserved;
            }

            this->ownerId = nullptr;
            return Types::errorType::ok;
        }

        std::map<std::string, std::string>
        Ticket::toData()
        {
            std::map<std::string, std::string> data;
            data["id"] = std::to_string(this->id.toInteger());
            data["sector"] = std::to_string(this->sector);
            data["row"] = std::to_string(this->row);
            data["seat"] = std::to_string(this->seat);
            data["eventId"] = std::to_string(this->eventId.toInteger());
            data["ownerId"] = (this->ownerId != nullptr) ? std::to_string(this->ownerId->toInteger()) : "";

            return data;
        }
    }
}
