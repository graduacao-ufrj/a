#ifndef CLIENT_HISTORY_H
#define CLIENT_HISTORY_H

#include "Types.h"
#include "IntegerId.h"
#include "DateTime.h"

#include <ctime>
#include <map>

namespace TicketOs {
    namespace Domain {
        class ClientHistory {
            private:
                IntegerId id;
                IntegerId clientId;
                DateTime arrivalTime;
                DateTime *responseTime;
                DateTime *conclusionTime;
                IntegerId eventId;
                bool successful;

                ClientHistory(
                    IntegerId id,
                    IntegerId clientId,
                    DateTime arrivalTime,
                    DateTime *responseTime,
                    DateTime *conclusionTime,
                    IntegerId eventId,
                    bool successful = false
                );

            public:
                //~ClientHistory();

                static ClientHistory create(
                    IntegerId id,
                    IntegerId clientId,
                    IntegerId eventId
                );

                static ClientHistory fromPersistence(std::map<std::string, std::string> data);

                bool wasSuccessful();
                bool isResponded();
                bool isConcluded();

                IntegerId getId();
                IntegerId getClientId();
                DateTime getArrivalTime();
                DateTime *getResponseTime();
                DateTime *getConclusionTime();
                IntegerId getEventId();

                Types::errorType setResponseTime();
                Types::errorType setConclusionTime();
                Types::errorType setSuccessful();
                Types::errorType setFailed();

                std::map<std::string, std::string> toData();
        };
    }
}

#endif