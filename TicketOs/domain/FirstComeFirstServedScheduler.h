#ifndef FIFO_H
#define FIFO_H

#include "Scheduler.h"
#include "Client.h"
#include "ClientHistory.h"
#include "Event.h"

#include <vector>
#include <thread>
#include <mutex>
#include <chrono>
#include <condition_variable>

namespace TicketOs {
    namespace Domain {
        class FirstComeFirstServedScheduler : public Scheduler {
            private:
                std::vector<TicketOs::Domain::Client> clients;
                int maximumConcurrentClients = 1;
                std::mutex &printLock;
            public:
                FirstComeFirstServedScheduler(
                    std::vector<TicketOs::Domain::Client> clients,
                    std::mutex &printLock,
                    int maximumConcurrentClients = 1
                );

                ~FirstComeFirstServedScheduler() override = default;

                void run(
                    Event event
                ) override;
                static void processClient(
                    Client *client,
                    ClientHistory *clientHistory,
                    int *numberOfConcurrentClients,
                    int *numberOfFinishedClients,
                    int *numberOfStarvingClients,
                    int maximumConcurrentClients,
                    int maximumNumberOfTickets,
                    std::mutex &concurrentLock,
                    std::condition_variable_any &concurrentCondition,
                    std::mutex &printLock
                );
        };
    }
}
#endif