#include "ShortestJobFirstScheduler.h"

namespace TicketOs {
    namespace Domain {
        ShortestJobFirstScheduler::ShortestJobFirstScheduler(
            std::vector<TicketOs::Domain::Client> clients,
            std::mutex &printLock,
            int maximumConcurrentClients
        ) : clients(clients),
        maximumConcurrentClients(maximumConcurrentClients),
        printLock(printLock) {}

        void ShortestJobFirstScheduler::run(Event event) {
            this->printLock.lock();
            std::cout << Utility::miliTimer() << "\t[INFO] Running Shortest Job First Scheduler" << std::endl;
            this->printLock.unlock();

            Infrastructure::BinaryIdRepository idRepository;
            Infrastructure::BinaryClientRepository clientRepository;

            std::vector<ClientHistory *> clientHistories;

            std::mutex concurrentLock;
            std::condition_variable_any concurrentCondition;
            std::vector<std::thread> clientThreads;

            int numberOfConcurrentClients = 0;
            int numberOfSuccessfulClients = 0;
            int numberOfStarvingClients = 0;
            const std::size_t totalNumberOfClients = clients.size();

            this->printLock.lock();
            std::cout << Utility::miliTimer() << "\t[INFO] Total number of tickets: " << event.getAvailableTickets().size() << std::endl;
            this->printLock.unlock();

            this->printLock.lock();
            std::cout << Utility::miliTimer() << "\t[INFO] Ordering clients by usage time" << std::endl;
            this->printLock.unlock();

            /**
             * -------------------------------------------------------------------------------------------------------------
             */
            std::sort(clients.begin(), clients.end(), [](Client a, Client b) {
                return a.getUsageTime().count() < b.getUsageTime().count();
            });

            std::chrono::time_point<std::chrono::system_clock> startTime = std::chrono::system_clock::now();
            for (std::size_t i = 0; i < totalNumberOfClients; i++) {
                TicketOs::Domain::Client *client = nullptr;
                client = &clients[i];

                ClientHistory *clientHistory = new ClientHistory(ClientHistory::create(
                    idRepository.nextId(),
                    client->getId(),
                    event.getId()
                ));

                clientHistories.push_back(clientHistory);

                while (numberOfConcurrentClients >= maximumConcurrentClients) {
                    /** Stop creating new threads if the maximum number of concurrent clients has been reached */
                    concurrentLock.lock();
                    numberOfStarvingClients += client->waitInLine(
                        std::ref(concurrentLock),
                        std::ref(concurrentCondition),
                        std::ref(this->printLock)
                    );
                    concurrentLock.unlock();
                }

                clientThreads.push_back(std::thread(
                    ShortestJobFirstScheduler::processClient,
                    client,
                    clientHistory,
                    &numberOfConcurrentClients,
                    &numberOfSuccessfulClients,
                    &numberOfStarvingClients,
                    this->maximumConcurrentClients,
                    event.getAvailableTickets().size(),
                    std::ref(concurrentLock),
                    std::ref(concurrentCondition),
                    std::ref(this->printLock)
                ));
            }

            for (std::size_t i = 0; i < totalNumberOfClients; i++) {
                clientThreads[i].join();
                clients[i].addHistory(*clientHistories[i]);
            }

            std::chrono::time_point<std::chrono::system_clock> endTime = std::chrono::system_clock::now();

            std::chrono::duration<double> elapsedSeconds = endTime - startTime;

            this->printLock.lock();
            std::cout << Utility::miliTimer() << "\t[INFO] All clients have been processed in " << std::to_string(elapsedSeconds.count() * 1000) << "ms" << std::endl;
            this->printLock.unlock();

            std::chrono::milliseconds meanResponseTime = std::chrono::milliseconds(0);
            std::chrono::milliseconds meanConclusionTime = std::chrono::milliseconds(0);
            for (ClientHistory *clientHistory : clientHistories) {
                meanResponseTime += (*(clientHistory->getResponseTime()) - clientHistory->getArrivalTime());
                meanConclusionTime += (*(clientHistory->getConclusionTime()) - clientHistory->getArrivalTime());
            }

            meanResponseTime /= clientHistories.size();
            meanConclusionTime /= clientHistories.size();

            this->printLock.lock();
            std::cout << Utility::miliTimer() << "\t[INFO] Mean response time: " << std::to_string(meanResponseTime.count()) << "ms" << std::endl;
            std::cout << Utility::miliTimer() << "\t[INFO] Mean conclusion time: " << std::to_string(meanConclusionTime.count()) << "ms" << std::endl;
            std::cout << Utility::miliTimer() << "\t[INFO] Number of starved clients: " << std::to_string(numberOfStarvingClients) << std::endl;
            this->printLock.unlock();

            for (Client client : clients) {
                clientRepository.save(client);
            }
        }

        void ShortestJobFirstScheduler::processClient(
            Client *client,
            ClientHistory *clientHistory,
            int *numberOfConcurrentClients,
            int *numberOfFinishedClients,
            int *numberOfStarvingClients,
            int maximumConcurrentClients,
            int maximumNumberOfTickets,
            std::mutex &concurrentLock,
            std::condition_variable_any &concurrentCondition,
            std::mutex &printLock
        ) {
            concurrentLock.lock();
            printLock.lock();
            std::cout << Utility::miliTimer() << "\t\t[INFO] Number of finished clients: " << *numberOfFinishedClients << std::endl;
            std::cout << Utility::miliTimer() << "\t\t[INFO] Maximum number of tickets: " << maximumNumberOfTickets << std::endl;
            printLock.unlock();
            if (*numberOfFinishedClients >= maximumNumberOfTickets) {
                printLock.lock();
                std::cout << Utility::miliTimer() << "\t\t[WARNING] All tickets have been sold!" << std::endl;
                printLock.unlock();

                concurrentCondition.notify_one();
                concurrentLock.unlock();
                return;
            }

            (*numberOfConcurrentClients)++;
            clientHistory->setResponseTime();
            printLock.lock();
            std::cout << Utility::miliTimer() << "\t\t[INFO] Number of concurrent clients: " << *numberOfConcurrentClients << std::endl;
            printLock.unlock();
            concurrentLock.unlock();

            printLock.lock();
            std::cout << Utility::miliTimer() << "\t\t[ACTION] Client " << client->getId().toInteger() << " with usage time " << client->getUsageTime().count() << "ms started" << std::endl;
            printLock.unlock();
            while (client->buyingTicket()) {
                // Do aditional work
            }
            printLock.lock();
            std::cout << Utility::miliTimer() << "\t\t[ACTION] Client " << client->getId().toInteger() << " with usage time " << client->getUsageTime().count() << "ms finished" << std::endl;
            printLock.unlock();

            concurrentLock.lock();
            clientHistory->setConclusionTime();
            
            if (*numberOfFinishedClients >= maximumNumberOfTickets) {
                clientHistory->setFailed();
                printLock.lock();
                std::cout << Utility::miliTimer() << "\t\t[WARNING] Client " << client->getId().toInteger() << " failed to buy a ticket" << std::endl;
                printLock.unlock();
                concurrentCondition.notify_one();
                concurrentLock.unlock();
                return;
            }

            clientHistory->setSuccessful();
            (*numberOfFinishedClients)++;
            (*numberOfConcurrentClients)--;
            concurrentCondition.notify_one();
            concurrentLock.unlock();
        }
    }
}