#ifndef CLIENT_H
#define CLIENT_H

#include "Types.h"
#include "IntegerId.h"
#include "ClientHistory.h"

#include <map>
#include <string>
#include <vector>
#include <chrono>
#include <mutex>
#include <condition_variable>

namespace TicketOs {
    namespace Domain {
        class Client {
            private:
                TicketOs::Domain::IntegerId id;
                TicketOs::Types::numberOfStars stars;
                std::chrono::milliseconds timeToBuyTicket;
                std::chrono::milliseconds usageTime;
                std::chrono::time_point<std::chrono::system_clock> *startTime = nullptr;
                std::chrono::time_point<std::chrono::system_clock> *waitStartTime = nullptr;
                unsigned int numberOfWaits = 0;
                std::vector<TicketOs::Domain::ClientHistory> clientHistories;

                Client(
                    TicketOs::Domain::IntegerId id,
                    TicketOs::Types::numberOfStars stars,
                    std::chrono::milliseconds usageTime,
                    std::vector<TicketOs::Domain::ClientHistory> clientHistories
                );
            public:
                /**
                 * @brief Create a Client object
                 */
                static Client create(
                    TicketOs::Domain::IntegerId id,
                    TicketOs::Types::numberOfStars stars,
                    std::chrono::milliseconds usageTime
                );
                /**
                 * @brief Create a Client object from an array of strings
                 */
                static Client fromPersistence(
                    std::map<std::string, std::string> data,
                    std::vector<std::map<std::string, std::string>> clientHistoriesData
                );

                TicketOs::Domain::IntegerId getId();
                TicketOs::Types::numberOfStars getStars();
                Types::errorType setStars(TicketOs::Types::numberOfStars stars);
                std::vector<TicketOs::Domain::ClientHistory> getClientHistories();

                Types::errorType addHistory(TicketOs::Domain::ClientHistory history);

                std::map<std::string, std::string> toData();

                std::chrono::milliseconds getUsageTime();
                std::chrono::time_point<std::chrono::system_clock> *getStartTime();

                bool buyingTicket();
                void waitUntilBuyingTicketIsDone();
                std::chrono::milliseconds getHowMuchTimeIsLeft();

                bool waitInLine(
                    std::mutex &concurrentLock,
                    std::condition_variable_any &concurrentCondition,
                    std::mutex &printLock
                );

                void pause();
                void resume();
                std::chrono::milliseconds getTimeToBuyTicket();

                /**
                 * Operators to compare two Client objects based on their number of stars
                 */
                bool operator==(const Client& other);
                bool operator>(const Client& other);
                bool operator<(const Client& other);
        };
    }
}

#endif