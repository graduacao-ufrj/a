#ifndef SHORTEST_COMPLETION_SCHEDULER_H
#define SHORTEST_COMPLETION_SCHEDULER_H

#include "Scheduler.h"

#include "Client.h"
#include "ClientHistory.h"
#include "IntegerId.h"

#include "../utility/miliTimer.h"
#include "../infrastructure/binary/BinaryIdRepository.h"

#include <vector>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <iostream>
#include <random>
#include <algorithm>
#include <list>

namespace TicketOs
{
    namespace Domain
    {
        class ShortestCompletionScheduler : public Scheduler {
            private:
                std::vector<Client> originalClients;
                unsigned int maximumConcurrentClients = 1;
                unsigned int numberOfFinishedClients = 0;
                std::mutex &printLock;
            public:
                ShortestCompletionScheduler(
                    std::vector<Client> originalClients,
                    int maximumConcurrentClients,
                    std::mutex &printLock
                );

                ~ShortestCompletionScheduler() override = default;

                void run(
                    Event event
                ) override;

                static void process(
                    IntegerId id,
                    std::list<Client *> &clients,
                    std::list<IntegerId> &finishedClients,
                    unsigned int &numberOfStarvingClients,
                    unsigned int &numberOfFinishedClients,
                    unsigned int maximumNumberOfClients,
                    unsigned int maximumNumberOfTickets,
                    std::mutex &queueLock,
                    std::condition_variable_any &queueChangeCondition,
                    std::condition_variable_any &waitingClientProcessCondition,
                    std::mutex &printLock
                );

                static void watcher(
                    std::vector<Client> &clients,
                    std::list<IntegerId> &finishedClients,
                    unsigned int &numberOfFinishedClients,
                    unsigned int maximumNumberOfClients,
                    std::mutex &queueLock,
                    std::condition_variable_any &queueChangeCondition,
                    std::condition_variable_any &waitingClientProcessCondition,
                    std::mutex &printLock
                );
        };
    }
}

#endif