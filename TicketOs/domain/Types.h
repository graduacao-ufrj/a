#ifndef TYPES_H
#define TYPES_H

#include <ctime>

namespace TicketOs {
    namespace Types {
        typedef unsigned short numberOfStars;

        typedef enum {
            ok,
            ticketNotReserved,
            ticketAlreadyReserved,
            ticketClientNotValid,
            clientHistoryAlreadyResponded,
            clientHistoryResponseTimeBeforeArrivalTime,
            clientHistoryResponseTimeNull,
            clientHistoryAlreadyConcluded,
            clientHistoryConclusionTimeBeforeArrivalTime,
            clientHistoryConclusionTimeBeforeResponseTime,
            clientHistoryConclusionTimeNull,
            clientHistoryNotResponded,
            clientClientHistoryIdDoesNotMatch,

            binaryIdRepositoryFileNotFound,
            binaryClientRepositoryFileNotFound,
            binaryTicketRepositoryFileNotFound,
            binaryTicketRepositoryFileCouldNotBeCreated,
            binaryEventRepositoryFileNotFound,
            binaryEventRepositoryFileCouldNotBeCreated,
            errorAmount
        } errorType;

        typedef enum {
            honorTribune,
            vipArea,
            associatesArea,
            boxes,
            nonAssociatesArea,
            visitorArea,
            pressArea,
            sectorMissing,
            sectorAmount
        } sectorType;
    }
}

#endif