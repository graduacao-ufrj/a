#include "miliTimer.h"

namespace TicketOs {
    namespace Utility {
        unsigned long long miliTimer() {
            static auto last = std::chrono::high_resolution_clock::now();
            auto now = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(now - last);

            last = now;
            return duration.count();
        }
    }
}