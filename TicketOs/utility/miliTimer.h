#ifndef MILI_TIMER_H
#define MILI_TIMER_H

#include <chrono>

namespace TicketOs {
    namespace Utility {
        unsigned long long miliTimer();
    }
}

#endif