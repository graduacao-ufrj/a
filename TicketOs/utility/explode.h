#ifndef EXPLODE_H
#define EXPLODE_H

#include <string>
#include <vector>

namespace TicketOs {
    namespace Utility {
        std::vector<std::string> explode(std::string str, char delimiter);
    }
}

#endif