#ifndef IMPLODE_H
#define IMPLODE_H

#include <string>
#include <vector>

namespace TicketOs {
    namespace Utility {
        std::string implode(std::vector<std::string> vector, std::string delimiter);
    }
}

#endif