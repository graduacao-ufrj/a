#include "explode.h"

namespace TicketOs {
    namespace Utility {
        std::vector<std::string> explode(std::string str, char delimiter) {
            std::vector<std::string> result;
            std::string current = "";
            for (char c : str) {
                if (c == delimiter) {
                    result.push_back(current);
                    current = "";
                } else {
                    current += c;
                }
            }
            result.push_back(current);
            return result;
        }
    }
}