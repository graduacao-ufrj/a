#include "implode.h"

#include <string>
#include <vector>

namespace TicketOs {
    namespace Utility {
        std::string implode(std::vector<std::string> vector, std::string delimiter)
        {
            std::string result = "";
            for (size_t i = 0; i < vector.size(); i++) {
                result += vector[i] + ((i != vector.size() - 1) ? delimiter : "");
            }
            return result;
        }
    }
}