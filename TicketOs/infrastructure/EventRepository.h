#ifndef EVENT_REPOSITORY_H
#define EVENT_REPOSITORY_H

#include "../domain/Event.h"
#include "../domain/Types.h"

#include <vector>

namespace TicketOs {
    namespace Infrastructure {
        class EventRepository {
            private:
                std::string path;
            public:
                virtual Types::errorType save(Domain::Event event) = 0;
                virtual Domain::Event findById(Domain::IntegerId eventId) = 0;
                virtual std::vector<Domain::Event> findAll() = 0;
                virtual Types::errorType commit() = 0;
        };
    }
}

#endif