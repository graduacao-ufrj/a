#include "BinaryEventRepository.h"

#include "../../utility/explode.h"
#include "../../utility/implode.h"
#include "BinaryTicketRepository.h"

#include <fstream>
#include <map>
#include <vector>
#include <string>

namespace TicketOs {
    namespace Infrastructure {
        BinaryEventRepository::BinaryEventRepository(std::string path, std::string ticketPath): path(path), ticketPath(ticketPath) {
            this->repository = {};
            this->dataWasFetched = false;

            std::ifstream file(path);
            if (!file.is_open()) {
                if (this->commit() != Types::errorType::ok) {
                    throw std::runtime_error("Failed to create the binary file for storing Event data");
                }
            }
            file.close();

            this->ticketRepository = BinaryTicketRepository(ticketPath);
        }

        Domain::Event
        BinaryEventRepository::findById(Domain::IntegerId eventId)
        {
            if (!this->dataWasFetched) {
                this->findAll();
            }

            for (Domain::Event event : this->repository) {
                if (event.getId() == eventId) {
                    return event;
                }
            }

            throw std::runtime_error("Event not found");
        }

        std::vector<Domain::Event>
        BinaryEventRepository::findAll()
        {
            std::ifstream file(path, std::ios::binary);
            if (!file.is_open()) {
                throw std::runtime_error("Failed to open the binary file for storing Event data");
            }

            std::vector<Domain::Ticket> tickets = this->ticketRepository.findAll();

            std::vector<std::string> keys = Utility::explode(DEFAULT_BINARY_EVENT_HEADERS, ',');
            std::string line;
            
            while (std::getline(file, line)) {
                std::map<std::string, std::string> data;
                if (line == DEFAULT_BINARY_EVENT_HEADERS) {
                    continue;
                }

                std::vector<std::string> values = Utility::explode(line, ',');
                for (long unsigned int i = 0; i < keys.size(); i++) {
                    if (values[i] == "") {
                        continue;
                    }
                    data[keys[i]] = values[i];
                }

                Domain::IntegerId eventId = Domain::IntegerId::fromInteger(std::stoi(data["id"]));
                std::vector<std::map<std::string, std::string>> eventTickets = {};
                for (Domain::Ticket ticket : tickets) {
                    if (ticket.getEventId() == eventId) {
                        eventTickets.push_back(ticket.toData());
                    }
                }
                
                Domain::Event event = Domain::Event::fromPersistence(data, eventTickets);
                this->repository.push_back(event);
            }

            file.close();

            this->dataWasFetched = true;
            return this->repository;
        }

        Types::errorType
        BinaryEventRepository::save(Domain::Event event)
        {
            if (!this->dataWasFetched) {
                this->findAll();
            }

            unsigned int index = 0;
            for (Domain::Event e : this->repository) {
                if (e.getId() == event.getId()) {
                    this->repository[index] = event;
                    return Types::errorType::ok;
                }
                index++;
            }

            this->repository.push_back(event);

            for (Domain::Ticket ticket : event.getAvailableTickets()) {
                this->ticketRepository.save(ticket);
            }
            return Types::errorType::ok;
        }

        Types::errorType
        BinaryEventRepository::commit()
        {
            std::ofstream file(path);
            if (!file.is_open()) {
                return Types::errorType::binaryEventRepositoryFileNotFound;
            }

            file << DEFAULT_BINARY_EVENT_HEADERS << std::endl;
            for (Domain::Event event : this->repository) {
                std::vector<std::string> values = {};
                std::vector<std::string> keys = Utility::explode(DEFAULT_BINARY_EVENT_HEADERS, ',');
                for (std::string key : keys) {
                    values.push_back(event.toData()[key]);
                }
                std::string line = Utility::implode(values, ",");
                file << line << std::endl;
            }

            file.close();

            this->ticketRepository.commit();

            return Types::errorType::ok;
        }

        bool
        BinaryEventRepository::hasFetchedData()
        {
            return this->dataWasFetched;
        }
    }
}