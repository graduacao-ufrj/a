#ifndef BINARY_CLIENT_REPOSITORY_H
#define BINARY_CLIENT_REPOSITORY_H

#include "../ClientRepository.h"
#include "../../domain/Client.h"
#include "../../domain/Types.h"
#include "../../domain/IntegerId.h"

#include <string>
#include <vector>

#ifndef TESTING
#define DEFAULT_BINARY_CLIENT_REPOSITORY_PATH "data/clients.csv"
#else
#define DEFAULT_BINARY_CLIENT_REPOSITORY_PATH "test_clients.csv"
#endif
#define DEFAULT_BINARY_CLIENT_HEADERS "id,stars"

#ifndef TESTING
#define DEFAULT_BINARY_CLIENT_HISTORY_REPOSITORY_PATH "data/client_histories.csv"
#else
#define DEFAULT_BINARY_CLIENT_HISTORY_REPOSITORY_PATH "test_client_histories.csv"
#endif
#define DEFAULT_BINARY_CLIENT_HISTORY_HEADERS "id,clientId,arrivalTime,responseTime,conclusionTime,eventId,successful"

namespace TicketOs {
    namespace Infrastructure {
        class BinaryClientRepository : public ClientRepository {
        private:
            std::string path = DEFAULT_BINARY_CLIENT_REPOSITORY_PATH;
            std::string historyPath = DEFAULT_BINARY_CLIENT_HISTORY_REPOSITORY_PATH;
            std::vector<Domain::Client> repository = {};
            bool dataWasFetched = false;
        public:
            BinaryClientRepository(std::string path = DEFAULT_BINARY_CLIENT_REPOSITORY_PATH, std::string historyPath = DEFAULT_BINARY_CLIENT_HISTORY_REPOSITORY_PATH);
            Types::errorType save(Domain::Client client);
            Domain::Client findById(Domain::IntegerId clientId);
            std::vector<Domain::Client> findAll();
            Types::errorType commit();
            bool hasFetchedData();
        };
    }
}

#endif