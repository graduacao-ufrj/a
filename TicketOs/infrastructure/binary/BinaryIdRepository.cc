#include "BinaryIdRepository.h"

#include "../../domain/Types.h"
#include "../../domain/IntegerId.h"

#include <fstream>
#include <string>
#include <stdexcept>

namespace TicketOs {
    namespace Infrastructure {
        BinaryIdRepository::BinaryIdRepository(std::string path): path(path) {
            Types::errorType error = readFromFile();

            if (error != Types::errorType::ok) {
                this->nextIdValue = 0;
                writeToFile();
            }
        }

        Types::errorType BinaryIdRepository::readFromFile() {
            std::ifstream file(path, std::ios::binary);
            if (!file.is_open()) {
                return Types::errorType::binaryIdRepositoryFileNotFound;
            }


            file >> nextIdValue;
            file.close();

            return Types::errorType::ok;
        }

        Types::errorType BinaryIdRepository::writeToFile() {
            std::ofstream file(this->path, std::ios::binary);
            if (!file.is_open()) {
                return Types::errorType::binaryIdRepositoryFileNotFound;
            }

            file << nextIdValue;
            file.close();

            return Types::errorType::ok;
        }

        Domain::IntegerId BinaryIdRepository::nextId() {
            this->nextIdValue++;


            Types::errorType error = writeToFile();


            if (error != Types::errorType::ok) {
                throw std::invalid_argument("The file could not be written");
            }


            return Domain::IntegerId::fromInteger(this->nextIdValue);
        }
    }
}