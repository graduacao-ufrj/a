#ifndef BINARY_EVENT_REPOSITORY_H
#define BINARY_EVENT_REPOSITORY_H

#include "../EventRepository.h"

#include "../../domain/Event.h"
#include "../../domain/Types.h"

#include "BinaryTicketRepository.h"

#include <string>
#include <vector>

#ifndef TESTING
#define EVENT_REPOSITORY_PATH "data/events.csv"
#else
#define EVENT_REPOSITORY_PATH "test_events.csv"
#endif
#define DEFAULT_BINARY_EVENT_HEADERS "id,dateTime,homeTeam,awayTeam,schedulerId"

namespace TicketOs {
    namespace Infrastructure {
        class BinaryEventRepository : public EventRepository {
            private:
                BinaryTicketRepository ticketRepository;

                std::string path;
                std::string ticketPath;
                std::vector<Domain::Event> repository = {};
                bool dataWasFetched = false;
            public:
                BinaryEventRepository(std::string path = EVENT_REPOSITORY_PATH, std::string ticketPath = DEFAULT_BINARY_TICKET_REPOSITORY_PATH);
                Types::errorType save(Domain::Event event);
                Domain::Event findById(Domain::IntegerId eventId);
                std::vector<Domain::Event> findAll();
                Types::errorType commit();
                bool hasFetchedData();
        };
    }
}

#endif