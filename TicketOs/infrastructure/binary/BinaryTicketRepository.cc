#include "BinaryTicketRepository.h"

#include "../../domain/Ticket.h"
#include "../../domain/Types.h"
#include "../../utility/explode.h"
#include "../../utility/implode.h"

#include <vector>
#include <string>
#include <fstream>

#include <iostream>

namespace TicketOs {
    namespace Infrastructure {
        BinaryTicketRepository::BinaryTicketRepository(std::string path): path(path)
        {
            this->repository = {};
            this->dataWasFetched = false;

            // Check if the file exists, if not create it
            std::ifstream file(path);
            if (!file.is_open()) {
                // Use an empty repository commit method to create the file
                if (this->commit() != Types::errorType::ok) {
                    throw std::runtime_error("Failed to create the binary file for storing Ticket data");
                }
            }
            file.close();
        }

        Domain::Ticket
        BinaryTicketRepository::findById(Domain::IntegerId ticketId)
        {
            if (!this->dataWasFetched) {
                this->findAll();
            }

            // Basic linear searching
            for (Domain::Ticket ticket : this->repository) {
                if (ticket.getId() == ticketId) {
                    return ticket;
                }
            }

            throw std::runtime_error("Ticket not found");
        }

        std::vector<Domain::Ticket>
        BinaryTicketRepository::findAll()
        {
            std::ifstream file(path);
            if (!file.is_open()) {
                throw std::runtime_error("Failed to open the binary file for storing Ticket data");
            }

            std::vector<std::string> keys = Utility::explode(DEFAULT_BINARY_TICKET_HEADERS, ',');
            std::string line;
            
            while (std::getline(file, line)) {
                std::map<std::string, std::string> data;
                if (line == DEFAULT_BINARY_TICKET_HEADERS) {
                    // Ignore the header
                    continue;
                }

                std::vector<std::string> values = Utility::explode(line, ',');
                for (long unsigned int i = 0; i < keys.size(); i++) {
                    if (values[i] == "") {
                        continue;
                    }
                    data[keys[i]] = values[i];
                }

                this->repository.push_back(
                    Domain::Ticket::fromPersistence(data)
                );
            }

            file.close();

            this->dataWasFetched = true;
            return this->repository;
        }

        Types::errorType
        BinaryTicketRepository::save(Domain::Ticket ticket)
        {
            if (!this->dataWasFetched) {
                this->findAll();
            }

            unsigned int index = 0;
            // Find the ticket in the repository
            for (Domain::Ticket t : this->repository) {
                if (t.getId() == ticket.getId()) {
                    this->repository[index] = ticket;
                    return Types::errorType::ok;
                }
                index++;
            }

            // Ticket not found, add it to the repository
            this->repository.push_back(ticket);
            return Types::errorType::ok;
        }

        Types::errorType
        BinaryTicketRepository::commit()
        {
            std::ofstream file(this->path);
            if (!file.is_open()) {
                return Types::errorType::binaryTicketRepositoryFileCouldNotBeCreated;
            }

            file << DEFAULT_BINARY_TICKET_HEADERS << std::endl;
            for (Domain::Ticket ticket : this->repository) {
                // Get the values in the order of the headers
                // It is not guaranteed that the order of the keys in the map is the same as the headers
                std::vector<std::string> values = {};
                std::vector<std::string> keys = Utility::explode(DEFAULT_BINARY_TICKET_HEADERS, ',');
                for (std::string key : keys) {
                    values.push_back(ticket.toData()[key]);
                }
                // Write the values in a string separated by commas
                std::string line = Utility::implode(values, ",");
                file << line << std::endl;
            }

            file.close();
            return Types::errorType::ok;
        }

        bool
        BinaryTicketRepository::hasFetchedData()
        {
            return this->dataWasFetched;
        }
    }
}