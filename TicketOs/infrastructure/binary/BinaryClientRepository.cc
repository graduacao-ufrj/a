#include "BinaryClientRepository.h"

#include "../../utility/explode.h"
#include "../../utility/implode.h"

#include <fstream>
#include <map>
#include <vector>
#include <string>

namespace TicketOs {
    namespace Infrastructure {
        BinaryClientRepository::BinaryClientRepository(std::string path, std::string historyPath): path(path), historyPath(historyPath) {
            this->repository = {};
            this->dataWasFetched = false;

            std::ifstream file(path);
            if (!file.is_open()) {
                if (this->commit() != Types::errorType::ok) {
                    throw std::runtime_error("Failed to create the binary file for storing Client data");
                }
            }
            file.close();
        }

        Domain::Client
        BinaryClientRepository::findById(Domain::IntegerId clientId)
        {
            if (!this->dataWasFetched) {
                this->findAll();
            }

            for (Domain::Client client : this->repository) {
                if (client.getId() == clientId) {
                    return client;
                }
            }

            throw std::runtime_error("Client not found");
        }

        std::vector<Domain::Client>
        BinaryClientRepository::findAll()
        {
            std::ifstream file(path, std::ios::binary);
            if (!file.is_open()) {
                throw std::runtime_error("Failed to open the binary file for storing Client data");
            }

            std::vector<std::string> keys = Utility::explode(DEFAULT_BINARY_CLIENT_HEADERS, ',');
            std::string line;
            
            while (std::getline(file, line)) {
                std::map<std::string, std::string> data;
                if (line == DEFAULT_BINARY_CLIENT_HEADERS) {
                    continue;
                }

                std::vector<std::string> values = Utility::explode(line, ',');
                for (long unsigned int i = 0; i < keys.size(); i++) {
                    if (values[i] == "") {
                        continue;
                    }
                    data[keys[i]] = values[i];
                }

                Domain::IntegerId id = Domain::IntegerId::fromInteger(std::stoi(data["id"]));
                
                // Get the client histories -------------------------------------
                std::vector<std::map<std::string, std::string>> histories = {};

                std::ifstream historyFile(this->historyPath, std::ios::binary);
                if (!historyFile.is_open()) {
                    throw std::runtime_error("File Client History for BinaryClientRepository not found");
                }

                std::vector<std::string> historyKeys = Utility::explode(DEFAULT_BINARY_CLIENT_HISTORY_HEADERS, ',');
                std::string historyLine;

                while (std::getline(historyFile, historyLine)) {
                    std::map<std::string, std::string> historyData;
                    if (historyLine == DEFAULT_BINARY_CLIENT_HISTORY_HEADERS) {
                        continue;
                    }

                    std::vector<std::string> historyValues = Utility::explode(historyLine, ',');
                    for (long unsigned int i = 0; i < historyKeys.size(); i++) {
                        if (historyValues[i] == "") {
                            continue;
                        }
                        historyData[historyKeys[i]] = historyValues[i];
                    }

                    if (std::stoul(historyData["clientId"]) == (unsigned long) id.toInteger()) {
                        histories.push_back(historyData);
                    }
                }

                historyFile.close();
                // ---------------------------------------------------------------
                
                // Create the client
                Domain::Client client = Domain::Client::fromPersistence(data, histories);

                this->repository.push_back(client);
            }

            file.close();

            this->dataWasFetched = true;
            return this->repository;
        }

        Types::errorType
        BinaryClientRepository::save(Domain::Client client)
        {
            if (!this->dataWasFetched) {
                this->findAll();
            }

            unsigned int index = 0;
            for (Domain::Client c : this->repository) {
                if (c.getId() == client.getId()) {
                    this->repository[index] = client;
                    return Types::errorType::ok;
                }
                index++;
            }

            this->repository.push_back(client);
            return Types::errorType::ok;
        }

        Types::errorType
        BinaryClientRepository::commit()
        {
            std::ofstream file(this->path);
            if (!file.is_open()) {
                return Types::errorType::binaryClientRepositoryFileNotFound;
            }

            file << DEFAULT_BINARY_CLIENT_HEADERS << std::endl;
            for (Domain::Client client : this->repository) {
                std::vector<std::string> values = {};
                std::vector<std::string> keys = Utility::explode(DEFAULT_BINARY_CLIENT_HEADERS, ',');
                for (std::string key : keys) {
                    values.push_back(client.toData()[key]);
                }
                std::string line = Utility::implode(values, ",");
                file << line << std::endl;
            }
            
            file.close();

            // Commit the client histories -----------------------------------------
            std::ofstream historyFile(this->historyPath);
            if (!historyFile.is_open()) {
                return Types::errorType::binaryClientRepositoryFileNotFound;
            }

            historyFile << DEFAULT_BINARY_CLIENT_HISTORY_HEADERS << std::endl;
            for (Domain::Client client : this->repository) {
                for (Domain::ClientHistory history : client.getClientHistories()) {
                    std::vector<std::string> values = {};
                    std::vector<std::string> keys = Utility::explode(DEFAULT_BINARY_CLIENT_HISTORY_HEADERS, ',');
                    for (std::string key : keys) {
                        values.push_back(history.toData()[key]);
                    }
                    std::string line = Utility::implode(values, ",");
                    historyFile << line << std::endl;
                }
            }

            historyFile.close();
            // ---------------------------------------------------------------------

            return Types::errorType::ok;
        }

        bool
        BinaryClientRepository::hasFetchedData()
        {
            return this->dataWasFetched;
        }
    }
}