#ifndef BINARY_TICKET_REPOSITORY_H
#define BINARY_TICKET_REPOSITORY_H

#include "../TicketRepository.h"

#include "../../domain/Ticket.h"
#include "../../domain/Types.h"

#include <vector>
#include <string>

#ifndef TESTING
#define DEFAULT_BINARY_TICKET_REPOSITORY_PATH "data/tickets.csv"
#else
#define DEFAULT_BINARY_TICKET_REPOSITORY_PATH "test_tickets.csv"
#endif
#define DEFAULT_BINARY_TICKET_HEADERS "id,sector,row,seat,eventId,ownerId"


namespace TicketOs {
    namespace Infrastructure {
        class BinaryTicketRepository : public TicketRepository {
            private:
                std::string path;
                std::vector<Domain::Ticket> repository;
                bool dataWasFetched = false;
            public:
                BinaryTicketRepository(std::string path = DEFAULT_BINARY_TICKET_REPOSITORY_PATH);

                Types::errorType save(Domain::Ticket ticket) override;
                Domain::Ticket findById(Domain::IntegerId ticketId) override;
                std::vector<Domain::Ticket> findAll() override;
                Types::errorType commit() override;

                bool hasFetchedData();
        };
    }
}

#endif