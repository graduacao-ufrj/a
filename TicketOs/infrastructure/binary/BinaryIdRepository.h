#ifndef BINARY_ID_REPOSITORY_H
#define BINARY_ID_REPOSITORY_H

#include "../IdRepository.h"
#include "../../domain/Types.h"
#include "../../domain/IntegerId.h"

#include <string>

#ifndef TESTING
#define DEFAULT_BINARY_ID_REPOSITORY_PATH "data/id_sequence"
#else
#define DEFAULT_BINARY_ID_REPOSITORY_PATH "test_id_sequence"
#endif

namespace TicketOs {
    namespace Infrastructure {
        class BinaryIdRepository : public IdRepository {
        private:
            std::string path;
            unsigned int nextIdValue;

            Types::errorType readFromFile();
            Types::errorType writeToFile();
        public:
            BinaryIdRepository(std::string path = DEFAULT_BINARY_ID_REPOSITORY_PATH);
            Domain::IntegerId nextId();
        };
    }
}

#endif