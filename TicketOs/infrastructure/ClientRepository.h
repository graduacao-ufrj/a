#ifndef CLIENT_REPOSITORY_H
#define CLIENT_REPOSITORY_H

#include "../domain/Client.h"
#include "../domain/Types.h"

#include <vector>

namespace TicketOs {
    namespace Infrastructure {
        class ClientRepository {
            private:
                std::string path;
                std::string historyPath;
            public:
                virtual Types::errorType save(Domain::Client client) = 0;
                virtual Domain::Client findById(Domain::IntegerId clientId) = 0;
                virtual std::vector<Domain::Client> findAll() = 0;
                virtual Types::errorType commit() = 0;
        };
    }
}
#endif
