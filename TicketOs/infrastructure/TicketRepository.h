#ifndef TICKET_REPOSITORY_H
#define TICKET_REPOSITORY_H

#include "../domain/Ticket.h"
#include "../domain/Types.h"

#include <vector>
#include <string>

namespace TicketOs {
    namespace Infrastructure {
        class TicketRepository {
            private:
                std::string path;
            public:
                virtual Types::errorType save(Domain::Ticket ticket) = 0;
                virtual Domain::Ticket findById(Domain::IntegerId ticketId) = 0;
                virtual std::vector<Domain::Ticket> findAll() = 0;
                virtual Types::errorType commit() = 0;
        };
    }
}

#endif