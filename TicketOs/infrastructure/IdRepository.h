#ifndef ID_REPOSITORY_H
#define ID_REPOSITORY_H

#include "../domain/IntegerId.h"

namespace TicketOs {
    namespace Infrastructure {
        class IdRepository {
        public:
            virtual Domain::IntegerId nextId() = 0;
        };
    }
}

#endif