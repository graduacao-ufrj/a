#include <iostream>

#include "TicketOs/domain/Client.h"
#include "TicketOs/domain/Ticket.h"
#include "TicketOs/domain/Event.h"
#include "TicketOs/domain/IntegerId.h"
#include "TicketOs/domain/ClientHistory.h"
#include "TicketOs/domain/Types.h"

#include "TicketOs/domain/TicketService.h"

#include "TicketOs/domain/FirstComeFirstServedScheduler.h"
#include "TicketOs/domain/ShortestJobFirstScheduler.h"
#include "TicketOs/domain/ShortestCompletionScheduler.h"

#include "TicketOs/infrastructure/binary/BinaryClientRepository.h"
#include "TicketOs/infrastructure/binary/BinaryIdRepository.h"
#include "TicketOs/infrastructure/binary/BinaryTicketRepository.h"
#include "TicketOs/infrastructure/binary/BinaryEventRepository.h"

#include "TicketOs/utility/miliTimer.h"

#include "TicketOs/domain/Scheduler.h"

#include <getopt.h>

#include <iostream>
#include <random>

int main(int argc, char **argv) {
    int *client = (int *) malloc(sizeof(int));
    int *ticket = (int *) malloc(sizeof(int));
    int *event = (int *) malloc(sizeof(int));
    int *updateClient = (int *) malloc(sizeof(int));
    int *updateTicket = (int *) malloc(sizeof(int));
    int *listAvailableTicketForEvent = (int *) malloc(sizeof(int));
    int *firstComeFirstServed = (int *) malloc(sizeof(int));
    int *shortestJobFirst = (int *) malloc(sizeof(int));
    int *shortestCompletionFirst = (int *) malloc(sizeof(int));

    std::random_device randomDevice;
    std::mt19937 randomEngine(randomDevice());
    std::uniform_int_distribution<int> randomDistribution(1, 100);

    TicketOs::Infrastructure::BinaryClientRepository clientRepository;
    TicketOs::Infrastructure::BinaryIdRepository idRepository;
    TicketOs::Infrastructure::BinaryTicketRepository ticketRepository;
    TicketOs::Infrastructure::BinaryEventRepository eventRepository;

    std::mutex printLock;

    int option;
    struct option longOptions[] = {
        {"client", no_argument, client, 1},
        {"ticket", no_argument, ticket, 1},
        {"update-client", no_argument, updateClient, 1},
        {"update-ticket", no_argument, updateTicket, 1},
        {"event", no_argument, event, 1},
        {"list-available-tickets-for-event", no_argument, listAvailableTicketForEvent, 1},
        // -------------------------------------------------------------------------------------------
        {"first-come-first-served", no_argument, firstComeFirstServed, 1},
        {"shortest-job-first", no_argument, shortestJobFirst, 1},
        {"shortest-completion-first", no_argument, shortestCompletionFirst, 1},
        {nullptr, 0, nullptr, 0}
    };

    *client = 0;
    *ticket = 0;
    *event = 0;
    *updateClient = 0;
    *updateTicket = 0;
    *listAvailableTicketForEvent = 0;
    *firstComeFirstServed = 0;
    *shortestJobFirst = 0;
    *shortestCompletionFirst = 0;

    while ((option = getopt_long(argc, argv, "", longOptions, nullptr)) != -1) {
    }

    if (*client) {
        if (optind >= argc) {
            std::cerr << "You must provide the number of clients to create" << std::endl;
            return 1;
        }

        int numberOfClients = std::stoi(argv[optind]);

        TicketOs::Domain::Client *client = (TicketOs::Domain::Client *) malloc(sizeof(TicketOs::Domain::Client));

        for (int i = 0; i < numberOfClients; i++) {
            *client = TicketOs::Domain::Client::create(
                idRepository.nextId(),
                randomDistribution(randomEngine) % 5,
                std::chrono::milliseconds(randomDistribution(randomEngine))
            );

            TicketOs::Domain::ClientHistory history = TicketOs::Domain::ClientHistory::create(
                idRepository.nextId(),
                client->getId(),
                TicketOs::Domain::IntegerId::fromInteger(1)
            );

            history.setResponseTime();
            history.setConclusionTime();
            if (randomDistribution(randomEngine) % 2 == 0) {
                history.setFailed();
            } else {
                history.setSuccessful();
            }

            client->addHistory(history);

            clientRepository.save(*client);
        }
        clientRepository.commit();

        delete client;
    }

    if (*event) {
        TicketOs::Domain::Event *event = (TicketOs::Domain::Event *) malloc(sizeof(TicketOs::Domain::Event));

        *event = TicketOs::Domain::Event::createStandard(
            idRepository.nextId(),
            TicketOs::Domain::DateTime::now(),
            idRepository.nextId(),
            "Home Team",
            "Away Team"
        );

        eventRepository.save(*event);
        eventRepository.commit();

        delete event;
    }

    if (*updateClient) {
        // Check for argument
        if (optind >= argc) {
            std::cerr << "You must provide a client id to update" << std::endl;
            return 1;
        }

        try {
            TicketOs::Domain::IntegerId clientId = TicketOs::Domain::IntegerId::fromInteger(std::stoi(argv[optind]));

            TicketOs::Domain::Client client = clientRepository.findById(clientId);

            TicketOs::Types::numberOfStars newStars = randomDistribution(randomEngine) % 5;
            while (newStars == client.getStars()) {
                newStars = randomDistribution(randomEngine) % 5;
            }
            client.setStars(newStars);

            clientRepository.save(client);
            clientRepository.commit();
        } catch (std::runtime_error e) {
            std::cerr << "Error: " << e.what() << std::endl;
            return 1;
        }
    }

    if (*updateTicket) {
        // Check for argument
        if (optind >= argc - 1) {
            std::cerr << "You must provide a ticket id and a client id to update" << std::endl;
            return 1;
        }

        try {
            TicketOs::Domain::IntegerId ticketId = TicketOs::Domain::IntegerId::fromInteger(std::stoi(argv[optind]));
            TicketOs::Domain::IntegerId clientId = TicketOs::Domain::IntegerId::fromInteger(std::stoi(argv[optind + 1]));

            TicketOs::Domain::Ticket ticket = ticketRepository.findById(ticketId);
            TicketOs::Domain::Client client = clientRepository.findById(clientId);

            ticket.release();
            ticket.reserve(new TicketOs::Domain::IntegerId(client.getId()));

            ticketRepository.save(ticket);
            ticketRepository.commit();
        } catch (std::runtime_error e) {
            std::cerr << "Error: " << e.what() << std::endl;
            return 1;
        }
    }

    if (*listAvailableTicketForEvent) {
        // Check for argument
        if (optind >= argc) {
            std::cerr << "You must provide an event id to list available tickets" << std::endl;
            return 1;
        }

        try {
            TicketOs::Domain::IntegerId eventId = TicketOs::Domain::IntegerId::fromInteger(std::stoi(argv[optind]));

            TicketOs::Domain::Event event = eventRepository.findById(eventId);

            std::vector<TicketOs::Domain::Ticket> availableTickets = event.getAvailableTickets();

            std::cout << TicketOs::Utility::miliTimer() << "Available tickets for event "
            << eventId.toInteger() << "1 " << event.getHomeTeam()
            << " X " << event.getAwayTeam() << ": " << availableTickets.size()
            << std::endl;

        } catch (std::runtime_error e) {
            std::cerr << "Error: " << e.what() << std::endl;
            return 1;
        }
    }

    /** ----------------- SCHEDULERS ----------------- */

    if (*firstComeFirstServed) {
        std::cout << TicketOs::Utility::miliTimer() << "[INFO] Initializing first come first served scheduler" << std::endl;
        if (optind + 1 >= argc) {
            std::cerr << "[ERROR] You must provide the maximum number of concurrent clients and number of clients" << std::endl;
            return 1;
        }

        int maximumNumberOfConcurrentClients = std::stoi(argv[optind]);

        if (maximumNumberOfConcurrentClients > std::thread::hardware_concurrency()) {
            maximumNumberOfConcurrentClients = std::thread::hardware_concurrency();
            std::cout << TicketOs::Utility::miliTimer() << "[WARNING] Maximum number of concurrent clients adjusted to " << maximumNumberOfConcurrentClients << std::endl;
        }

        int numberOfClients = std::stoi(argv[optind + 1]);

        std::cout << TicketOs::Utility::miliTimer() << "[INFO] Available number of threads: " << std::thread::hardware_concurrency() << std::endl;

        std::vector<TicketOs::Domain::Client> clients;
        for (int i = 0; i < numberOfClients; i++) {
            TicketOs::Domain::Client client = TicketOs::Domain::Client::create(
                TicketOs::Domain::IntegerId::fromInteger(i),
                randomDistribution(randomEngine) % 5,
                std::chrono::milliseconds(randomDistribution(randomEngine))
            );
            std::cout << TicketOs::Utility::miliTimer() << "[INFO] Client " << i << " created" << std::endl;

            clients.push_back(client);
        }

        TicketOs::Domain::Scheduler *scheduler = new TicketOs::Domain::FirstComeFirstServedScheduler(
            clients,
            printLock,
            maximumNumberOfConcurrentClients
        );
        std::cout << TicketOs::Utility::miliTimer() << "[INFO] Scheduler created" << std::endl;

        TicketOs::Domain::Event event = TicketOs::Domain::Event::createStandard(
            idRepository.nextId(),
            TicketOs::Domain::DateTime::now(),
            idRepository.nextId(),
            "Home Team",
            "Away Team"
        );
        std::cout << TicketOs::Utility::miliTimer() << "[INFO] Event created" << std::endl;

        eventRepository.save(event);

        scheduler->run(
            event
        );

        //eventRepository.commit();
        //clientRepository.commit();
    }

    if (*shortestJobFirst) {
        std::cout << TicketOs::Utility::miliTimer() << "[INFO] Initializing shortest job first scheduler" << std::endl;
        if (optind + 1 >= argc) {
            std::cerr << "[ERROR] You must provide the maximum number of concurrent clients and number of clients" << std::endl;
            return 1;
        }

        int maximumNumberOfConcurrentClients = std::stoi(argv[optind]);

        if (maximumNumberOfConcurrentClients > std::thread::hardware_concurrency()) {
            maximumNumberOfConcurrentClients = std::thread::hardware_concurrency();
            std::cout << TicketOs::Utility::miliTimer() << "[WARNING] Maximum number of concurrent clients adjusted to " << maximumNumberOfConcurrentClients << std::endl;
        }

        int numberOfClients = std::stoi(argv[optind + 1]);

        std::cout << TicketOs::Utility::miliTimer() << "[INFO] Available number of threads: " << std::thread::hardware_concurrency() << std::endl;

        std::vector<TicketOs::Domain::Client> clients;
        for (int i = 0; i < numberOfClients; i++) {
            TicketOs::Domain::Client client = TicketOs::Domain::Client::create(
                TicketOs::Domain::IntegerId::fromInteger(i),
                randomDistribution(randomEngine) % 5,
                std::chrono::milliseconds(randomDistribution(randomEngine))
            );
            std::cout << TicketOs::Utility::miliTimer() << "[INFO] Client " << i << " created" << std::endl;

            clients.push_back(client);
        }

        TicketOs::Domain::Scheduler *scheduler = new TicketOs::Domain::ShortestJobFirstScheduler(
            clients,
            printLock,
            maximumNumberOfConcurrentClients
        );
        std::cout << TicketOs::Utility::miliTimer() << "[INFO] Scheduler created" << std::endl;

        TicketOs::Domain::Event event = TicketOs::Domain::Event::createStandard(
            idRepository.nextId(),
            TicketOs::Domain::DateTime::now(),
            idRepository.nextId(),
            "Home Team",
            "Away Team"
        );

        eventRepository.save(event);

        scheduler->run(
            event
        );

        //eventRepository.commit();
        //clientRepository.commit();
    }

    if (*shortestCompletionFirst) {
        std::cout << TicketOs::Utility::miliTimer() << "[INFO] Initializing shortest completion first scheduler" << std::endl;
        if (optind + 1 >= argc) {
            std::cerr << "[ERROR] You must provide the maximum number of concurrent clients and number of clients" << std::endl;
            return 1;
        }

        int maximumNumberOfConcurrentClients = std::stoi(argv[optind]);

        if (maximumNumberOfConcurrentClients > std::thread::hardware_concurrency()) {
            maximumNumberOfConcurrentClients = std::thread::hardware_concurrency();
            std::cout << TicketOs::Utility::miliTimer() << "[WARNING] Maximum number of concurrent clients adjusted to " << maximumNumberOfConcurrentClients << std::endl;
        }

        int numberOfClients = std::stoi(argv[optind + 1]);

        std::cout << TicketOs::Utility::miliTimer() << "[INFO] Available number of threads: " << std::thread::hardware_concurrency() << std::endl;

        std::vector<TicketOs::Domain::Client> clients;
        for (int i = 0; i < numberOfClients; i++) {
            TicketOs::Domain::Client client = TicketOs::Domain::Client::create(
                TicketOs::Domain::IntegerId::fromInteger(i),
                randomDistribution(randomEngine) % 5,
                std::chrono::milliseconds(randomDistribution(randomEngine))
            );
            std::cout << TicketOs::Utility::miliTimer() << "[INFO] Client " << i << " created" << std::endl;

            clients.push_back(client);
        }

        TicketOs::Domain::Scheduler *scheduler = new TicketOs::Domain::ShortestCompletionScheduler(
            clients,
            maximumNumberOfConcurrentClients,
            printLock
        );
        std::cout << TicketOs::Utility::miliTimer() << "[INFO] Scheduler created" << std::endl;

        TicketOs::Domain::Event event = TicketOs::Domain::Event::createStandard(
            idRepository.nextId(),
            TicketOs::Domain::DateTime::now(),
            idRepository.nextId(),
            "Home Team",
            "Away Team"
        );

        eventRepository.save(event);

        scheduler->run(
            event
        );

        //eventRepository.commit();
        //clientRepository.commit();
    }

    return 0;
}