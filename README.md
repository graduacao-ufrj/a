# Ticket OS

## How to compile
```bash
$ make
$ ./TicketOs --help
```

## How to test
```bash
$ make test
```
*You need to have Bazel installed in your system*

[toc]

## Objetivo 
O objetivo do projeto é simular um sistema operacional com diversos tipos de escalonamento de processsamento diferentes e obter métricas a respeito do seu desempenho para que possam ser comparados entre si. Nesse sistema, temos n threads de processamento diferentes que lidarão com a demanda de m processos ou "tarefas", onde n é o número de "vendedores de ticket" e m o número de "clientes" que comprar ingressos para um evento.


## Organização do código

### domain

É onde ficam todos os diferentes escalonadores usados, bem como os módulos principais para definir o comportamento e processamento dos clientes e o tratamento de dados para a comparação entre escalonadores. 


### utility

Essa pasta é usada para módulos extras utilitários com funções usadas para melhor tratar o sistema operacional, incluindo uma função para timer de alta resolução que é usada para gravar o tempo que um cliente está sendo atendido. 


## Assunção de dados do cliente

Embora não utilizado em todos os escalonadores, a classe Cliente usada é a mesma em todos, e todas as classes cliente possuem os seguintes atributos:

- id - para identificação de cada objeto cliente
- timeToBuyTicket - tempo que o cliente "espera" na fila para ser processado
- usageTime - tempo que o cliente leva para ser processado
- ClientHistory - classe usada para armazenar os dados do cliente junto com cada evento que acontece [espera na fila, atendimento e saída da fila, possível entrada na fila novamente] (como um log). Dentro do módulo ClientHistory também é feito um apanhado de excessões para tratamento de erros. 

## Escalonadores implementados

- First In, First Out (FIFO) - Fila comum onde o primeiro que chega é o primeiro a ser executado e a terminar
- Shortest Time-to-Completion First (STCF) - prioriza sempre os processos mais rápidos primeiro, assumindo que se sabe quanto tempo cada um levará
- Shortest Job First (SJF) - prioriza sempre os processos que atualmente estão mais próximos de serem concluídos
- Round-Robin (RR) - alterna entre o atendimento de múltiplos clientes em pequenos intervalos de tempo chamados "quantuns"
- Multi-Level Feedback Queue (MlFQ) - implementa níveis de prioridade para os clientes que são tratadas no escalonador: se o cliente tem prioridade maior, ele é atendido em vez de um com prioridade menor, e se ambos tem prioridade igual, rodam alternativamente como  Round-Robin. Assim como outras regras como: Um nova tarefa entra sempre na fila de maior prioridade, quando tarefa usa toda sua fatia justa ela vai para a próxima fila de menor prioridade e após um tempo S, mover todas tarefas para a mais alta prioridade.

## Resultados e discurssão

Pretendemos levar para a apresentação como que o sistema de ingressos desempenhou para cada um dos escalonadores, através de gráficos das medidas mencionadas que simulam o comportamento das filas.